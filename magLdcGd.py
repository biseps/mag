# Iterates over a ".extract" file generating 
# the quadratic LDC and GD coeffeicent 
# for binaries in a specified filter band


import getopt
import sys 
import biseps 
import magUtils 
import passFilters as p
import numpy as np


opts, args = getopt.getopt(sys.argv,'')

file_in = str(args[1])
sysType = 'targ'


x = biseps.readFile()


try:
    x.kepler5(file_in)
except:
    x.kepler4(file_in)


filtIn  = ['kepler']
filters = p.passFilters(filtIn)

l  = magUtils.calcLDC(filters)
g  = magUtils.calcGD(filters)
bb = magUtils.calcBC(filters)


# this gives a seg fault sometimes(?)
# kSize=np.count_nonzero(x.kep)

kSize = len(x.kep)


for i in range(kSize):
        ldc11, ldc12 = l.ldc(x.kep[i].t1, x.kep.logg1[i], x.kep.met[i], x.kep.kw1[i])
        ldc21, ldc22 = l.ldc(x.kep[i].t2, x.kep.logg2[i], x.kep.met[i], x.kep.kw2[i])

        gd1  = g.gd(x.kep[i].t1, x.kep.logg1[i], x.kep.met[i], x.kep.kw1[i])
        gd2  = g.gd(x.kep[i].t2, x.kep.logg2[i], x.kep.met[i], x.kep.kw2[i])

        bc1  = bb.bc(x.kep[i].t1, x.kep.logg1[i], x.kep.met[i], x.kep.kw1[i])
        bc2  = bb.bc(x.kep[i].t2, x.kep.logg2[i], x.kep.met[i], x.kep.kw2[i])

        mag1 = bb.calcMag(x.kep[i].l1, bc1)[0]
        mag2 = bb.calcMag(x.kep[i].l2, bc2)[0]
        #print mag1, mag2

        #print x.kep[i].t1, x.kep.logg1[i], x.kep.met[i], x.kep.kw1[i]
        #print x.kep[i].t2, x.kep.logg2[i], x.kep.met[i], x.kep.kw2[i]

        # output file
        print mag1, ldc11[0], ldc12[0], gd1[0], \
              mag2, ldc21[0], ldc22[0], gd2[0]


