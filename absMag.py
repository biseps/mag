import biseps as b
import numpy as np
import magUtils as m
import getopt
import sys as sys
import passFilters as p

opts, extra = getopt.getopt(sys.argv[1:],'')
file=str(extra[0])
isSingle=int(extra[1])


x=b.readFile()

try:
	x.kepler4(file)
except:
	x.kepler6(file)

filtIn=['kepler']
filters=p.passFilters(filtIn)
bb=m.calcBC(filters)
kSize=np.count_nonzero(x.kep)
	 
for i in range(kSize):
	bc1=bb.bc(x.kep[i].t1,x.kep.logg1[i],x.kep.met[i],x.kep.kw1[i])
	bc2=bb.bc(x.kep[i].t2,x.kep.logg2[i],x.kep.met[i],x.kep.kw2[i])

	m1=bb.calcMag(x.kep[i].l1,bc1)
	m2=bb.calcMag(x.kep[i].l2,bc2)

	mB=bb.magBin(x.kep[i].l1,x.kep[i].l2,bc1,bc2,isSingle)

	print x.kep.num[i],m1[0],m2[0],mB[0]
