import biseps as b
import numpy as np
import magUtils as m
import getopt
import sys as sys


opts, extra = getopt.getopt(sys.argv[1:],'')
#extra=["extract.1","ranLoc.dat.1","1"]	
kepFile=str(extra[0])
ranLocFile=str(extra[1])
isSingle=int(extra[2])


x=b.readFile()
	 
x.kepler5(kepFile)
x.ranLoc(ranLocFile)

filters=['310','311','312','313','314','505','355','356','357']
bb=m.calcBC(filters)
av=m.calcAV(filters)
kSize=np.count_nonzero(x.kep.m1)

met=10**(np.random.normal(-0.003,0.097,np.size(x.kep))+np.log10(0.02))

for i in range(kSize):
	
	bc1=bb.bc(x.kep[i].t1,x.kep.logg1[i],met[i],x.kep.kw1[i])
	bc2=bb.bc(x.kep[i].t2,x.kep.logg2[i],met[i],x.kep.kw2[i])
		
	mB=bb.magBin(x.kep[i].l1,x.kep[i].l2,bc1,bc2,isSingle)

	#We turn ext/0.88 ext is extinction in kepler not extinction V band so turn intno V band ext
	ext=av.avBin(x.kep[i].t1,x.kep[i].t2,x.kep[i].l1,x.kep[i].l2,x.kep.logg1[i],x.kep.logg2[i],x.kep[i].kw1,x.kep[i].kw2,x.ranLoc[i].ext/0.88,isSingle)

	mag=av.distCorrect(mB,x.ranLoc[i].dist,ext,x.ranLoc[i].ext/0.88)

	if mag[2]-mag[3] > 0.3:
		kp=0.7*mag[4]+0.3*mag[2]
	else:
		kp=0.75*mag[3]+0.25*mag[2]

	print x.kep[i].id,x.ranLoc[i].long,x.ranLoc[i].lat,met[i],' '.join([str(item) for item in mag]),kp

#print >> stderr x.kep[-1].id