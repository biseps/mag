MODULE magUtils
	IMPLICIT NONE
	
	
	INTEGER,PARAMETER :: filterLen=3,maxFilePath=80
	
	TYPE teffLoggVal
		DOUBLE PRECISION :: teff,logg
	END TYPE teffLoggVal
	
	TYPE indexTeff
		INTEGER :: id
		DOUBLE PRECISION :: teff
	END TYPE indexTeff

	TYPE :: interpol
		DOUBLE PRECISION :: x,y,z
	END TYPE interpol
	
    TYPE :: headerPars
		CHARACTER(len=filterLen) :: filter
		INTEGER :: num,fileSize
		CHARACTER(len=maxFilePath) :: filePath
    END TYPE headerPars
    
    TYPE :: arrayCoeffs
		INTEGER :: maxSize,numCols,numTypes,numFilters
    END TYPE arrayCoeFfs
    
    TYPE met
		TYPE(headerPars),DIMENSION(:),ALLOCATABLE :: bcHeader
		TYPE(arrayCoeffs) :: arrCoeffs
		
		CHARACTER(len=maxFilePath) :: configFile
		
		TYPE(headerPars),DIMENSION(:),ALLOCATABLE :: filterConfigPars
		TYPE(teffLoggVal),DIMENSION(:,:),ALLOCATABLE :: header
		DOUBLE PRECISION,DIMENSION(:,:,:,:),ALLOCATABLE :: bcs
		TYPE(arrayCoeffs) :: bcArrCoeffs
		TYPE(indexTeff),DIMENSION(:,:),ALLOCATABLE :: indexs
		INTEGER,DIMENSION(:),ALLOCATABLE :: bcIndexSize	
	END TYPE met
    
	DOUBLE PRECISION, PARAMETER :: solarToCGS=6.993328363d-5
	DOUBLE PRECISION, PARAMETER :: NewtGrav=6.67384d-11
	!Visual brightness of the sun
	!Defined in http://adsabs.harvard.edu/abs/2010AJ....140.1158T
	DOUBLE PRECISION, PARAMETER :: Vsun=-26.76
	DOUBLE PRECISION, PARAMETER :: teffSun=5777.d0
	DOUBLE PRECISION, PARAMETER :: Msun=1.98892d30,Rsun=6.955d8
	!Defined as Vsun+31.572+BC_v,sun derive BC from tables and use that
	DOUBLE PRECISION, PARAMETER :: BoloMagSun=4.75,zSun=0.02d0,alpha=0.d0

	CHARACTER(len=filterLen),DIMENSION(:),ALLOCATABLE :: filters
	
	CONTAINS
	
	SUBROUTINE preInt(configFile,filters,configPars,header,coeffs,arrCoeffs,ind,sizeIndex,rootPath)
		IMPLICIT NONE
		
		TYPE(arrayCoeffs),INTENT(INOUT) :: arrCoeffs
		CHARACTER(len=maxFilePath),INTENT(IN) :: configFile
		CHARACTER(len=filterLen),DIMENSION(1:arrCoeffs%numFilters),INTENT(IN) :: filters
		TYPE(headerPars),DIMENSION(:),ALLOCATABLE,INTENT(INOUT) :: configPars
		TYPE(teffLoggVal),DIMENSION(:,:),ALLOCATABLE,INTENT(INOUT) :: header
		DOUBLE PRECISION,DIMENSION(:,:,:,:),ALLOCATABLE,INTENT(INOUT) :: coeffs
		INTEGER,DIMENSION(:),ALLOCATABLE,INTENT(INOUT) :: sizeIndex
		TYPE(indexTeff),DIMENSION(:,:),ALLOCATABLE,INTENT(INOUT) :: ind
		CHARACTER(len=*) :: rootPath
		
		INTEGER :: configFileSize,i,j,aerr

		CALL readGlobalConfig(rootPath//configFile,configFileSize,configPars)
		
		
		arrCoeffs%maxSize=maxval(configPars%fileSize)
		arrCoeffs%numTypes=maxval(configPars%num)
		
		
		ALLOCATE(header(1:arrCoeffs%maxSize,1:arrCoeffs%numTypes),&
			coeffs(1:arrCoeffs%maxsize,1:arrCoeffs%numCols,1:arrCoeffs%numTypes,1:arrCoeffs%numfilters),&
			STAT=aerr)
			IF (aerr > 0) THEN
				WRITE(0,*) 'ERROR magnitudes.f90 preInt'
				WRITE(0,*) 'ERROR: Memory allocation failed for preInt magUtils!'
				WRITE(0,*) configFile,arrCoeffs%numTypes
				WRITE(0,*) 'END ERROR'
				STOP 
			END IF

		
		DO i=1,configFileSize
			!Read the teff and logg values
			IF(configPars(i)%filter=='000') THEN
				CALL getHeaderInfo(rootPath//configPars(i)%filePath,&
					configPars(i)%fileSize,header(1:configPars(i)%fileSize,configPars(i)%num))
			END IF
			
			!If filter matchs a required filter read in the coefficents
			DO j=1,arrCoeffs%numFilters
				IF(configPars(i)%filter==filters(j)) THEN
					CALL getData(rootPath//configPars(i)%filePath,&
						configPars(i)%fileSize,coeffs(1:configPars(i)%fileSize,1:arrCoeffs%numCols,configPars(i)%num,j),&
						arrCoeffs%numCols)
				END IF
			END DO
		END DO
		
		
		!Now to make indexs
		ALLOCATE(ind(1:arrCoeffs%maxSize,1:arrCoeffs%numTypes),sizeIndex(1:arrCoeffs%numTypes),&
			STAT=aerr)
			IF (aerr > 0) THEN
				WRITE(0,*) 'ERROR magnitudes.f90 preInt'
				WRITE(0,*) 'ERROR: Memory allocation failed for preInt magUtils indexing!'
				WRITE(0,*) arrCoeffs%maxSize,arrCoeffs%numTypes
				WRITE(0,*) 'END ERROR'
				STOP 
			END IF
	
		
		DO i=1,arrCoeffs%numTypes
			CALL indexArray(header(:,i)%teff,size(header(:,i)%teff),ind(1:arrCoeffs%maxSize,i),sizeIndex(i))
		END DO

	END SUBROUTINE preInt
	!!!!!!!!!!!!!!!!!!!!!!!!
	INTEGER FUNCTION getFileSize(fileName)
		IMPLICIT NONE
		LOGICAl :: fileExists
		CHARACTER(len=maxFilePath),INTENT(IN) :: fileName
		CHARACTER(len=1000) :: BUFFER
		
		fileExists=.false.
!  		write(0,*) fileName
		inquire(FILE=fileName(1:LEN_TRIM(fileName)),exist=fileExists)
		if(fileExists .eqv. .false.) then
			write(0,*) "No file ", fileName(1:LEN_TRIM(fileName))
			write(0,*) "getFileSize"
			stop
		end if
		OPEN(1,FILE=fileName(1:LEN_TRIM(fileName)),STATUS='OLD',ACTION='READ')
	! 	write(*,*) file

		getFileSize=0
		DO 
			read(1, '(A)', end=99)
			getFileSize=getFileSize+1
		ENDDO
99  	CONTINUE
		CLOSE(1)
		
	END FUNCTION getFileSize

	
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	
	!Reads a file for the bc/ldc/gd that has format:
	!At least one line must be (HH SPACE num SPACE path)
	!Where hh means its a header file (teff SPACE logg) 
	!Subsequent lines are (2 char filter SPACE num SPACE number of lines SPACE path)
	!Where num=0 for normal, 1 m giants, 2 WD for BCS'
	!Currently only 0 for everything else 
	SUBROUTINE readGlobalConfig(fileName,fileSize,dat)
		IMPLICIT NONE
		CHARACTER(len=maxFilePath),INTENT(IN) ::fileName
		INTEGER,INTENT(OUT) :: fileSize
		TYPE(headerPars),DIMENSION(:),ALLOCATABLE,INTENT(OUT) :: dat
		INTEGER :: aerr,i
		CHARACTER(len=100) :: test
		
		fileSize=getFileSize(fileName)
		ALLOCATE(dat(1:fileSize),STAT=aerr)
		IF (aerr > 0) THEN
			WRITE(0,*) 'ERROR magnitudes.f90 readGlobalConfig'
			WRITE(0,*) 'ERROR: Memory allocation failed for filterData!'
			WRITE(0,*) fileNAME,fileSize
			WRITE(0,*) 'END ERROR'
			STOP 
		END IF
		
		dat%num=0
		dat%fileSize=0
 		dat%filePath=''
 		dat%filter=''
		
		OPEN(1,FILE=fileName(1:LEN_TRIM(fileName)),STATUS='OLD',ACTION='READ')
		DO i=1,fileSize
 			READ(1,'(A,1X,I1,1X,I3,1X,A)') dat(i)%filter,dat(i)%num,dat(i)%fileSize,dat(i)%filePath
! 			write(*,*) test
		END DO
		CLOSE(1)	
		
	END SUBROUTINE readGlobalConfig

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	SUBROUTINE getHeaderInfo(fileName,fileSize,header)
		IMPLICIT NONE
		CHARACTER(len=*),INTENT(IN) ::fileName
		INTEGER,INTENT(IN) :: fileSize
		TYPE(teffLoggVal),DIMENSION(:),INTENT(OUT) :: header    
		INTEGER :: aerr,i

		
		OPEN(1,FILE=fileName(1:LEN_TRIM(fileName)),STATUS='OLD',ACTION='READ')
		DO i=1,fileSize
			READ(1,*) header(i)%teff,header(i)%logg
		END DO
		CLOSE(1)
	
	END SUBROUTINE
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	
	SUBROUTINE getData(fileName,fileSize,dataOut,numCols)
		IMPLICIT NONE
		CHARACTER(len=*),INTENT(IN) :: fileName
		INTEGER,INTENT(IN) :: fileSize
		DOUBLE PRECISION,DIMENSION(1:fileSize,1:numCols),INTENT(OUT) :: dataOut
		INTEGER,INTENT(IN) :: numCols
		INTEGER :: i,j,aerr
		
! 		write(*,*) fileName
		
		OPEN(1,FILE=fileName(1:LEN_TRIM(fileName)),STATUS='OLD',ACTION='READ')
		DO i=1,fileSize
			READ(1,*) (dataOut(i,j),j=1,numCols)
		END DO		
		CLOSE(1)		
		
	END SUBROUTINE getData 

	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	SUBROUTINE indexArray(teff,sizeArray,indexOut,sizeIndex)
		IMPLICIT NONE
		INTEGER,INTENT(IN) :: sizeArray
		DOUBLE PRECISION,DIMENSION(1:sizeArray),INTENT(IN) :: teff
		INTEGER,INTENT(OUT) :: sizeIndex
		INTEGER :: i,j
		TYPE(indexTeff),DIMENSION(1:sizeArray),INTENT(OUT) :: indexOut
		DOUBLE PRECISION :: tmp
		
		sizeIndex=0
		tmp=0.d0
		j=1
		DO  i=1,sizeArray
			if(teff(i) .gt. tmp) then
				!Store index point
				indexOut(j)%id=i
				indexOut(j)%teff=teff(i)
				tmp=teff(i)
				j=j+1
			endif
		enddo
		j=j-1

		sizeIndex=j

	END SUBROUTINE indexArray

	!Parameters:
	!radius, mass solar units
	!teff in kelvin
	!filters array of 2 character filters
	!Returns
	!res array same size as filters but with the corrections in
	SUBROUTINE getInterpolCoeff(r,m,teff,numFilters,res,header,sizeHeader,indexHeader,sizeIndexHeader,coeffs)

		IMPLICIT NONE

		DOUBLE PRECISION,INTENT(IN) :: r,m,teff
		DOUBLE PRECISION :: logg
		INTEGER :: p1,p2,p3,p4,i,j
		TYPE(interpol) :: pol1,pol2,pol3,pol4
		INTEGER,INTENT(IN) :: sizeHeader,sizeIndexHeader,numFilters
		DOUBLE PRECISION,DIMENSION(1:numFilters),INTENT(INOUT) :: res

		TYPE(teffLoggVal),DIMENSION(1:sizeHeader) :: header
		DOUBLE PRECISION,DIMENSION(1:sizeHeader,1:numFilters) :: coeffs
		
		TYPE(indexTeff),DIMENSION(1:sizeIndexHeader):: indexHeader
		
			
		p1=0;p2=0;p3=0;p4=0

 		logg=calcLogg(m,r)
! 		logg=5.5d0
! 		write(*,*) indexHeader
		CALL getCoeff(teff,logg,header,sizeHeader,indexHeader,sizeIndexHeader,p1,p2,p3,p4)		
		pol1%x=header(p1)%teff
		pol2%x=header(p2)%teff
		pol3%x=header(p3)%teff
		pol4%x=header(p4)%teff
		
		pol1%y=header(p1)%logg
		pol2%y=header(p2)%logg
		pol3%y=header(p3)%logg
		pol4%y=header(p4)%logg

		DO j=1,numFilters
			!p1,p2,p3,p4 are array indices extract teff,logg and BC for Mag
			pol1%z=coeffs(p1,j)
			pol2%z=coeffs(p2,j)
			pol3%z=coeffs(p3,j)
			pol4%z=coeffs(p4,j)
			res(j)=bilinearInterpol(teff,logg,pol1,pol2,pol3,pol4)
		END DO

	
	END SUBROUTINE getInterpolCoeff	
	
	
	!!!!!!!!!!!!!!!!!!!!!!!!
	SUBROUTINE getCoeff(teffIn,loggIn,inputArr,inputSize,indexArr,indexSize,p1,p2,p3,p4)
		IMPLICIT NONE
		DOUBLE PRECISION,INTENT(IN) :: teffIn,loggIn
		TYPE(teffLoggVal),DIMENSION(1:inputSize),INTENT(IN) :: inputArr
		TYPE(indexTeff),DIMENSION(1:indexSize),INTENT(IN) :: indexArr
		INTEGER,INTENT(IN) :: inputSize,indexSize
		INTEGER :: minTeffIndex,maxTeffIndex,max2TeffIndex,TeffIndex,i
		INTEGER,INTENT(OUT) :: p1,p2,p3,p4

		p1=1;p2=1;p3=1;p4=1

		!Teff is lower than index goes 
		IF(teffIn .lt. indexArr(1)%teff) THEN
			p1=1
			!Next array point has next logg value
			p4=2
			!Next teff min logg
			p2=indexArr(2)%id
			!Next teff next min logg value
			p3=indexArr(2)%id+1
			RETURN
		END IF
		!Teff is too high
		IF(teffIn.gt.indexArr(indexSize)%teff)THEN
			!Shift to previous teff min logg
			p1=indexArr(indexSize-1)%id
			!Shift to previous teff but max logg which should be previous array element
			p4=indexArr(indexSize-1)%id

			p2=indexArr(indexSize)%id
			!Max teff usally only has 1 logg point
			p3=indexArr(indexSize)%id
			RETURN
		END IF

		!Log g range must be checked during search phase due to dependence on Teff

		!Peform linear search over the index array for Teff that straddle teffIn

		IF(abs(teffIn-indexArr(1)%teff).lt. abs(teffIn-indexArr(2)%teff)) THEN
			TeffIndex=2
		ELSE IF(abs(teffIn-indexArr(indexSize)%teff).lt. abs(teffIn-indexArr(indexSize-1)%teff)) THEN
			TeffIndex=indexSize-1
		ELSE
			DO i=2,indexSize-1
				IF(abs(teffIn-indexArr(i)%teff).le. abs(teffIn-indexArr(i-1)%teff) &
				.and. (abs(teffIn-indexArr(i)%teff).le. abs(teffIn-indexArr(i+1)%teff))) THEN
					TeffIndex=i
					EXIT
				END IF	
			END DO
		END IF
		

		!Teffindex hold the closest teff match, if we compare for > or < teff 
		!we can see if its minTeff or max Teff and set the other as the +-1 indice
		IF(teffIn .le. indexArr(TeffIndex)%teff) THEN
			!teff is smaller so max is teffIndex and min is TeffIndex-1
			maxTeffIndex=indexArr(TeffIndex)%id
			minTeffIndex=indexArr(TeffIndex-1)%id

			max2TeffIndex=indexArr(TeffIndex+1)%id
		ELSE
			!teff is bigger so min is teffIndex and max is TeffIndex+1
			maxTeffIndex=indexArr(TeffIndex+1)%id
			minTeffIndex=indexArr(TeffIndex)%id

			IF(TeffIndex+2 .gt. indexSize) THEN 
				max2TeffIndex=indexArr(indexSize)%id
			ELSE
				max2TeffIndex=indexArr(TeffIndex+2)%id
			END IF
		END IF

		!So we have the min and max Teff points now to get log g
		!Cant use index anymore must search inputArr

		!Find the min and max logg for min teff

		!Make sure logg is in range for min Teff
		IF (loggIn .lt. inputArr(minTeffIndex)%logg)THEN
			p1=minTeffIndex
			p4=minTeffIndex
		ELSE IF (loggIn .gt. inputArr(maxTeffIndex-1)%logg)THEN
			p1=maxTeffIndex-1
			p4=maxTeffIndex-1
		ELSE
			DO i=minTeffIndex,maxTeffIndex-1
				IF(loggIn .gt. inputArr(i)%logg) THEN
					p1=i
					p4=i+1
		! 				write(*,*) "i",i
				END IF
			END DO
		END IF


		!Make sure logg is in range for max Teff
		IF (loggIn .lt. inputArr(maxTeffIndex)%logg)THEN
			p2=maxTeffIndex
			p3=maxTeffIndex
		ELSE IF (loggIn .gt. inputArr(max2TeffIndex-1)%logg)THEN
			p3=max2TeffIndex-1
			p2=max2TeffIndex-1
		ELSE
			DO i=maxTeffIndex,max2TeffIndex-1
				IF(loggIn .gt. inputArr(i)%logg) THEN
					p2=i
					p3=i+1
				END IF
				
			END DO
		END IF

	END SUBROUTINE getCoeff

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	ELEMENTAL DOUBLE PRECISION FUNCTION linearInterpol(x,x0,x1,y0,y1)
		IMPLICIT NONE
		DOUBLE PRECISION,INTENT(IN) :: x,x0,x1,y0,y1

		linearInterpol=y0+(((x-x0)*y1)-((x-x0)*y0))/(x1-x0)

	END FUNCTION linearInterpol
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	! Perform a bilinear interpolation, the 4 points are the TYpes with an x,y and a value
	! The form a grid starting in bottom left corner and go counter clockwise
	DOUBLE PRECISION FUNCTION bilinearInterpol(xIn,yIn,p1,p2,p3,p4)
		IMPLICIT NONE
		TYPE(interpol),INTENT(IN)   :: p1,p2,p3,p4
		DOUBLE PRECISION :: x,y,t,u
		DOUBLE PRECISION,INTENT(IN) :: xIn,yIn
		DOUBLE PRECISION :: x1,x2,x3,x4,y1,y2,y3,y4
		DOUBLE PRECISION,PARAMETER :: smallInc=1.d-10,EPS=1.d-3

		x=xIn
		y=yIn

		!Shift x into range, 
		IF(x.lt.MIN(p1%x,p4%x)) x=MIN(p1%x,p4%x)
		IF(x.gt.MAX(p2%x,p3%x)) x=MAX(p2%x,p3%x)
		!Shift y into range,
		IF(y.lt.MIN(p1%y,p2%y)) y=MIN(p1%y,p2%y)
		IF(y.gt.MAX(p3%y,p4%y)) y=MAX(p3%y,p4%y)


		!Make sure the grid points are not the same (div by 0 otherwise)
		x1=MIN(p1%x,p4%x)
		x2=MAX(p2%x,p3%x)

		y1=MIN(p1%y,p2%y)
		y2=MAX(p4%y,p3%y)

		IF(x2-x1 .lt. EPS) x2=x2+EPS
		IF(y2-y1 .lt. EPS) y2=y2+EPS

		t=(x-x1)/(x2-x1)
		u=(y-y1)/(y2-y1)

		bilinearInterpol=((1.d0-t)*(1.d0-u)*p1%z)+(t*(1.d0-u)*p2%z)+(t*u*p3%z)+((1.d0-t)*u*p4%z)

	END FUNCTION bilinearInterpol

	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	! Calculates the surface gravity of a star
	! M in solar mass
	! R in soalr radii
	! logg returns in cm/s^2
	!Rfarmer 3/11/11
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	ELEMENTAL DOUBLE PRECISION FUNCTION calcLogg(m,r)
	IMPLICIT NONE
	DOUBLE PRECISION,INTENT(IN) :: m,r

	calcLogg=log10(100.d0*(NewtGrav*m*Msun)/(r*r*Rsun*Rsun))

	END FUNCTION calcLogg
	
	
	ELEMENTAL DOUBLE PRECISION FUNCTION z2met(z)
	IMPLICIT NONE
	DOUBLE PRECISION,INTENT(IN) :: z

	
	z2met=log10(z/zSun)+alpha
	END FUNCTION z2met
	
	
END MODULE magUtils
