MODULE typeDef
	USE spectral
	IMPLICIT NONE
	TYPE system
		INTEGER :: idB,idP,massTrans,isSingle,isSingleInt
		DOUBLE PRECISION,DIMENSION(1:2) :: m,r,t,l
		DOUBLE PRECISION :: porb,minTime,maxTime,age,mInt1,mInt2
		DOUBLE PRECISION :: a,met,long,lat,dist,ext
		CHARACTER(len=100) :: chn
		INTEGER,DIMENSION(1:2) :: kw
		TYPE(spectralType),DIMENSION(1:2) :: spec
		DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE :: BCs,col
  	END TYPE system


	INTERFACE OPERATOR (+)
		MODULE PROCEDURE mergeSys
	END INTERFACE OPERATOR (+)

	CONTAINS

	ELEMENTAL TYPE(system) FUNCTION mergeSys(biseps,pop)
		TYPE(system),INTENT(IN) :: biseps,pop

		mergeSys=biseps

		mergeSys%idP=pop%idP
		mergeSys%mInt1=pop%mInt1
		mergeSys%mInt2=pop%mInt2
		mergeSys%a=pop%a
		mergeSys%age=pop%age
		mergeSys%isSingleInt=pop%isSingle
		mergeSys%met=pop%met
		mergeSys%long=pop%long
		mergeSys%lat=pop%lat
		mergeSys%dist=pop%dist
		mergeSys%ext=pop%ext

	END FUNCTION mergeSys


END MODULE typeDef


PROGRAM mag
	USE BC
	USE AV
	USE magUtils
	USE spectral
	USE typeDef
	IMPLICIT NONE

	INTEGER :: i,j,lines,lineBiseps,linePop,aerr
! 	CHARACTER(len=3),DIMENSION(1:10) :: filters=(/'310','311','312','313','314','505','355','356','357','500'/)
	INTEGER,PARAMETER :: sizeFilters=10

	TYPE(system),DIMENSION(:),ALLOCATABLE :: sysB,sysP

	DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE :: magSing

	CHARACTER(len=120) :: finBiseps,finPop
	CHARACTER(len=25) :: formatOut
	CHARACTER(len=2) :: sFiltersChar

	!Input file from BiSEPS
	call get_command_argument(1,finBiseps)
	!Input file from pop
	call get_command_argument(2,finPop)
	!Get linecount
	lineBiseps=getFileSize(finBiseps)
	linePop=getFileSize(finPop)

	if(lineBiseps .gt. linePop) THEN
		WRITE(0,*)
		WRITE(0,*) 'BiSEPS out must have <= number of lines to pop out'
		WRITE(0,*) lineBiseps,linePop
		STOP 'Abnormal program termination!!!'		
	END IF
	lines=lineBiseps

! 	filters=(/310,311,312,313,314,505,355,356,357,500/)
	CALL preIntBC((/'310','311','312','313','314','505','355','356','357','500'/))
	CALL preIntCol((/'310','311','312','313','314','505','355','356','357','500'/))

	!Allocate arrays
	ALLOCATE(sysB(1:lineBiseps),sysP(1:linePop),STAT=aerr)
	IF (aerr > 0) THEN
		WRITE(0,*)
		WRITE(0,*) '1, ERROR ALLOCATING MEMORY!'
		WRITE(0,*)
		STOP 'Abnormal program termination!!!'
	END IF

	!Read File
	OPEN(1,file=finBiseps,STATUS='old',action='read')
	OPEN(2,file=finPop,STATUS='old',action='read')
	DO i=1,lineBiseps
		READ(1,fmt=101) sysB(i)%idB,sysB(i)%m(1),sysB(i)%r(1),sysB(i)%t(1),sysB(i)%l(1),&
						sysB(i)%m(2),sysB(i)%r(2),sysB(i)%t(2),sysB(i)%l(2),&
						sysB(i)%porb,sysB(i)%minTime,sysB(i)%maxTime,sysB(i)%massTrans,&
						sysB(i)%isSingle,sysB(i)%kw(1),sysB(i)%kw(2),sysB(i)%chn
	END DO
						
	DO i=1,linePop
		READ(2,*) sysP(i)%idP,sysP(i)%mInt1,sysP(i)%mInt2,sysP(i)%a,sysP(i)%age,sysP(i)%isSingleInt,&
						sysP(i)%met,sysP(i)%long,sysP(i)%lat,sysP(i)%dist,sysP(i)%ext
	END DO
	CLOSE(1)
	CLOSE(2)


	!Now we need to match the lines from the pop file into the biseps struct
	!+ is overloaded inside the typeDef module
	DO i=1,lineBiseps
		DO j=i,linePop
			IF(sysB(i)%idB==sysP(j)%idP)THEN
				sysB(i)=sysB(i)+sysP(j)
! 				write(*,*) i,j,sysB(i)%idB,sysP(j)%idP,sysB(i)%met
			END IF
		END DO
	END DO
	DEALLOCATE(sysP)
	

	write(sFiltersChar,'(I2)') sizeFilters
	formatOut='(I8,1X,'//sFiltersChar//'(E12.5,1X))'

	!Calc mag and colour correction
	DO i=1,lineBiseps
		CALL getMagBC(sysB(i)%m,sysB(i)%r,sysB(i)%t,sysB(i)%l,sysB(i)%kw,sysB(i)%met,sysB(i)%isSingle,magSing,sysB(i)%BCs,sysB(i)%spec)
		CALL getCol(sysB(i)%m,sysB(i)%r,sysB(i)%t,sysB(i)%l,sysB(i)%kw,sysB(i)%ext,sysB(i)%isSingle,sysB(i)%col)
		write(*,formatOut) sysB(i)%idB,sysB(i)%BCs+(5.0*dlog10(sysB(i)%dist))-5.0+(sysB(i)%ext*sysB(i)%col)
		DEALLOCATE(magSing)
	END DO

	
! 100  FORMAT (I8,1X,4(E12.5,1X),I1,1X,5(E12.5,1X))
 101  FORMAT (I8,1X,11(E12.5,1X),4(I2,1X),A)
 
END PROGRAM mag
