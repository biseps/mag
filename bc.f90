
!!!!!!!!!!!!!!!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	! Get BC corrections for Multiple bands 
	! Uses bi linear interpole over tables provided by
	! http://adsabs.harvard.edu/abs/2002A%26A...391..195G
	! http://adsabs.harvard.edu/abs/2004A%26A...422..205G
	! "Theoretical isochrones in several photometric systems
	!I. Johnson-Cousins-Glass, HST/WFPC2, HST/NICMOS, 
	!Washington, and ESO Imaging Survey filter sets"
	!by L. Girardi, G. Bertelli, A. Bressan, C. Chiosi, 
	!M.A.T. Groenewegen, P. Marigo, B. Salasnich, and A. Weiss,
	!2002, Astronomy & Astrophysics, in press (astro-ph/0205080)

	!UPDATE: Uses tables from Girardi L.et al, 2008, PASP, in press
	!(May 2008 issue) Revised bolometric corrections and extinction
	!coefficients for the ACS and WFPC2 photometric systems
	! Only small chnage in each coeff

	! R Farmer 04/08/2011
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE BC
	USE magUtils
	IMPLICIT NONE
	
	!Set up the  metalciities
	INTEGER,PARAMETER :: numMet=7,colNum=1
	DOUBLE PRECISION,DIMENSION(1:numMet) :: metVal=(/-2.5d0,-2.d0,-1.5d0,-1.d0,-0.5d0,0.d0,0.5d0/)
	TYPE(met),DIMENSION(1:numMet) :: allMet
	
	INTEGER,PARAMETER :: numCols=1,sunType=1
	!Suns abs magnitude
	DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE :: magSun,bcsun
	CHARACTER(len=filterLen),DIMENSION(:),ALLOCATABLE :: filtersBC
	CHARACTER(len=37) :: rootPathBC='/padata/beta/users/rfarmer/tables/BC3'

	
	CONTAINS
	
	SUBROUTINE getMagBCMet(mass,rad,teff,lum,startype,isSingle,magSing,magBin,metal)
		IMPLICIT NONE
		
		DOUBLE PRECISION,DIMENSION(1:2),INTENT(IN) :: mass,rad,teff,lum
		INTEGER,DIMENSION(1:2),INTENT(IN) :: startype
		TYPE(met),INTENT(INOUT) :: metal
		DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,INTENT(OUT) :: magSing
		DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE,INTENT(OUT) :: magBin
		INTEGER,INTENT(IN) :: isSingle

		DOUBLE PRECISION :: sunMag,lum1,lum2
		DOUBLE PRECISION,DIMENSION(1:2) :: tmp
		DOUBLE PRECISION,DIMENSION(1:metal%bcArrCoeffs%numFilters,1:2) :: BC
		INTEGER :: i,aerr,numFilters

		numFilters=metal%bcArrCoeffs%numFilters
		
		!Get BC corrrections in various pass bands
		CALL getInterpolCoeff(rad(1),mass(1),teff(1),numFilters,BC(:,1),&
		metal%header(:,startype(1)),metal%filterConfigPars(startype(1))%fileSize,&
		metal%indexs(:,startype(1)),metal%bcIndexSize(startype(1)),&
		metal%bcs(:,colNum,startype(1),1:numFilters))
		
		CALL getInterpolCoeff(rad(2),mass(2),teff(2),numFilters,BC(:,2),&
		metal%header(:,startype(2)),metal%filterConfigPars(startype(2))%fileSize,&
		metal%indexs(:,startype(2)),metal%bcIndexSize(startype(2)),&
		metal%bcs(:,colNum,startype(2),1:numFilters))
		
		ALLOCATE(magSing(1:numFilters,1:2),magBin(1:numFilters))
			IF (aerr > 0) THEN
				WRITE(0,*) 'ERROR magnitudes.f90 getMagBC'
				WRITE(0,*) 'ERROR: Memory allocation failed for magSing,magBin!'
				WRITE(0,*) numFilters
				WRITE(0,*) 'END ERROR'
				STOP
			END IF
			
		!Calc abs magnitude in each pass band for each star
		tmp=-2.5d0*log10(lum)+BoloMagSun
			
		DO i=1,numFilters
			magSing(i,:)=tmp-BC(i,:)
			magBin(i)=makeMagBin(magSing(i,:),magSun(i),isSingle)
		END DO

	END SUBROUTINE getMagBCMet

	PURE DOUBLE PRECISION FUNCTION makeMagBin(mag,sunM,isSingle)
		IMPLICIT NONE
		DOUBLE PRECISION,INTENT(IN),DIMENSION(1:2) :: mag
		DOUBLE PRECISION,INTENT(IN) :: sunM
		DOUBLE PRECISION :: lum1,lum2
		INTEGER,INTENT(IN) :: isSingle

		lum1=10.d0**(-(mag(1)-sunM)/2.5)
		lum2=10.d0**(-(mag(2)-sunM)/2.5)

		IF(isSingle==0) THEN
			makeMagBin=sunM-2.5*log10(lum1+lum2)
		ELSE
			makeMagBin=sunM-2.5*log10(lum1)
		END IF
	END FUNCTION makeMagBin
	
	SUBROUTINE preIntBC(inFilters)
		USE spectral
		IMPLICIT NONE
		CHARACTER(len=filterLen),DIMENSION(:),INTENT(IN) :: inFilters
		INTEGER :: i,aerr,numFilters
		
		!Set up bcArrCoeffs
		numFilters=size(inFilters)
		IF(numFilters == 0) THEN
			WRITE(0,*) 'ERROR magnitudes.f90 preIntBC'
			WRITE(0,*) 'ERROR: Need at least 1 filter'
			WRITE(0,*) inFilters,numFilters
			WRITE(0,*) 'END ERROR'
			STOP 	
		END IF
		allMet%bcArrCoeffs%numFilters=numFilters
		
		ALLOCATE(filtersBC(1:numFilters),magSun(1:numFilters),&
		bcSun(1:numFilters),STAT=aerr)
			IF (aerr > 0) THEN
				WRITE(0,*) 'ERROR magnitudes.f90 preIntBC'
				WRITE(0,*) 'ERROR: Memory allocation failed for preInt filters!'
				WRITE(0,*) inFilters,numFilters
				WRITE(0,*) 'END ERROR'
				STOP 
			END IF		
		
		filtersBC=inFilters
		
		
		!Set file path for each metalcitiy
		allMet(1)%configFile='/m25/bc_config.cfg'
		allMet(2)%configFile='/m20/bc_config.cfg'
		allMet(3)%configFile='/m15/bc_config.cfg'
		allMet(4)%configFile='/m10/bc_config.cfg'
		allMet(5)%configFile='/m05/bc_config.cfg'
		allMet(6)%configFile='/p00/bc_config.cfg'
		allMet(7)%configFile='/p05/bc_config.cfg'
		
		allMet%bcArrCoeffs%numCols=numCols
		
		!PreInt each met now
		DO i=1,numMet
			CALL preInt(allMet(i)%configFile,filtersBC,allMet(i)%filterConfigPars,allMet(i)%header,allMet(i)%bcs,&
					allMet(i)%bcArrCoeffs,allMet(i)%indexs,allMet(i)%bcIndexSize,rootPathBC)
		END DO
		
		!Generate abs mag for sun
		CALL getInterpolCoeff(1.d0,1.d0,teffSun,allMet(6)%bcArrCoeffs%numFilters,bcSun,&
			allMet(6)%header(:,sunType),&
			allMet(6)%filterConfigPars(sunType)%fileSize,&
			allMet(6)%indexs(:,sunType),&
			allMet(6)%bcIndexSize(sunType),&
			allMet(6)%bcs(:,numCols,sunType,1:allMet(6)%bcArrCoeffs%numFilters))
	
		magSun=boloMagSun-bcSun
! 		write(*,*) magSun,boloMagSun,bcSun
		
		!Pre int spectral typing 
		CALL preIntSpec()
	
	END SUBROUTINE preIntBC
	
	SUBROUTINE getMagBC(mass,rad,teff,lum,skw,metalicity,isSingle,magSing,magBin,spec)
		USE spectral
		IMPLICIT NONE
		DOUBLE PRECISION,DIMENSION(1:2),INTENT(IN) :: mass,rad,teff,lum
		DOUBLE PRECISION,INTENT(IN) :: metalicity
		INTEGER,DIMENSION(1:2),INTENT(IN) :: skw
		INTEGER,DIMENSION(1:2) :: startype
		DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,INTENT(INOUT) :: magSing
		DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE :: magSing1,magSing2
		DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE,INTENT(INOUT) :: magBin
		DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE :: magBin1,magBin2
		INTEGER,INTENT(IN) :: isSingle
		TYPE(spectralType),DIMENSION(1:2),INTENT(INOUT) :: spec
		
		DOUBLE PRECISION :: feh
		INTEGER :: i,aerr,numFilters

		feh=z2met(metalicity)	
		!Get star type
		CALL getStarType(mass,rad,teff,lum,skw,starType,spec)

		nuMFilters=allMet(1)%bcArrCoeffs%numFilters
!  		write(*,*) metalicity,feh,numFilters
		!Run if equal to excatly one of the metalicities
		DO i=1,numMet
			if(feh==metVal(i)) THEN
				CALL getMagBCMet(mass,rad,teff,lum,starType,isSingle,magSing,magBin,allMet(i))
				RETURN
			END IF
		END DO
		
		if(feh < metVal(1)) THEN
			!Run only at loweset met
			CALL getMagBCMet(mass,rad,teff,lum,starType,isSingle,magSing,magBin,allMet(1))
			RETURN	
		ELSE IF (feh > metVal(numMet)) THEN
			!Run at highest
			CALL getMagBCMet(mass,rad,teff,lum,starType,isSingle,magSing,magBin,allMet(numMet))
			RETURN
		ELSE
			!Run interpolated bwtween 2 mets
			DO i=1,numMet-1
				if(feh >= metVal(i) .and. feh <= metVal(i+1)) THEN

					numFilters=allMet(i)%bcArrCoeffs%numFilters
					ALLOCATE(magSing(1:numFilters,1:2),magBin(1:numFilters))
						IF (aerr > 0) THEN
							WRITE(0,*) 'ERROR magnitudes.f90 getMagBC'
							WRITE(0,*) 'ERROR: Memory allocation failed for magSing,magBin!'
							WRITE(0,*) numFilters
							WRITE(0,*) 'END ERROR'
							STOP
						END IF
					magSing=0.d0
					magBin=0.d0
				
					CALL getMagBCMet(mass,rad,teff,lum,starType,isSingle,magSing1,magBin1,allMet(i))
					CALL getMagBCMet(mass,rad,teff,lum,starType,isSingle,magSing2,magBin2,allMet(i+1))			
					!Interpolate over values
					magSing=linearInterpol(feh,metVal(i),metVal(i+1),magSing1,magSing2)
					magBin=linearInterpol(feh,metVal(i),metVal(i+1),magBin1,magBin2)

					RETURN
				END IF
			END DO
			write(*,*) "Error metallicity not found",metalicity,feh
			write(*,*) "Valid range",metVal(1),metVal(numMet)
			stop	
		END IF

		write(*,*) "Error metallicity not found",metalicity,feh
		write(*,*) "Valid range",metVal(1),metVal(numMet)
		stop
	
	END SUBROUTINE getMagBC
	
END MODULE BC