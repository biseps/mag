MODULE AV
	USE magUtils
	IMPLICIT NONE
	
	!Set up the  metalciities
	INTEGER,PARAMETER :: numExt=10,colNum=1
	DOUBLE PRECISION,DIMENSION(1:numExt) :: metExt=(/1.d0,2.d0,3.d0,4.d0,5.d0,6.d0,7.d0,8.d0,9.d0,10.d0/)
	TYPE(met),DIMENSION(1:numExt) :: allExt
	
	INTEGER,PARAMETER :: numCols_col=1
	CHARACTER(len=filterLen),DIMENSION(:),ALLOCATABLE :: filtersAv
	CHARACTER(len=42) :: rootPathAV='/padata/beta/users/rfarmer/tables/extinct/'

	CONTAINS

	SUBROUTINE getColCorrExt(mass,rad,teff,lum,startype,isSingle,metal,colCorr)
		IMPLICIT NONE
		
		DOUBLE PRECISION,DIMENSION(1:2),INTENT(IN) :: mass,rad,teff,lum
		INTEGER,DIMENSION(1:2),INTENT(IN) :: startype
		TYPE(met),INTENT(INOUT) :: metal
		INTEGER,INTENT(IN) :: isSingle

		DOUBLE PRECISION,DIMENSION(1:metal%bcArrCoeffs%numFilters,1:2) :: cols
		DOUBLE PRECISION,DIMENSION(1:metal%bcArrCoeffs%numFilters),INTENT(OUT) :: colCorr
		INTEGER :: i,aerr,numFilters

		numFilters=metal%bcArrCoeffs%numFilters
		
		!Get Color corrrections in various pass bands
		CALL getInterpolCoeff(rad(1),mass(1),teff(1),numFilters,cols(:,1),&
		metal%header(:,startype(1)),metal%filterConfigPars(startype(1))%fileSize,&
		metal%indexs(:,startype(1)),metal%bcIndexSize(startype(1)),&
		metal%bcs(:,colNum,startype(1),1:numFilters))
		
		CALL getInterpolCoeff(rad(2),mass(2),teff(2),numFilters,cols(:,2),&
		metal%header(:,startype(2)),metal%filterConfigPars(startype(2))%fileSize,&
		metal%indexs(:,startype(2)),metal%bcIndexSize(startype(2)),&
		metal%bcs(:,colNum,startype(2),1:numFilters))
		
		colCorr=(lum(1)/(SUM(LUM)))*cols(:,1)+(lum(2)/(SUM(LUM)))*cols(:,2)
		
		
	END SUBROUTINE getColCorrExt


	SUBROUTINE preIntCol(inFilters)
		USE spectral
		IMPLICIT NONE
		CHARACTER(len=filterLen),DIMENSION(:),INTENT(IN) :: inFilters
		INTEGER :: i,aerr,numFilters
		
		!Set up bcArrCoeffs
		numFilters=size(inFilters)
		IF(numFilters == 0) THEN
			WRITE(0,*) 'ERROR magnitudes.f90 preIntCol'
			WRITE(0,*) 'ERROR: Need at least 1 filter'
			WRITE(0,*) inFilters,numFilters
			WRITE(0,*) 'END ERROR'
			STOP 	
		END IF
		allExt%bcArrCoeffs%numFilters=numFilters
		
		ALLOCATE(filtersAV(1:numFilters),STAT=aerr)
			IF (aerr > 0) THEN
				WRITE(0,*) 'ERROR magnitudes.f90 preIntCol'
				WRITE(0,*) 'ERROR: Memory allocation failed for preInt filters!'
				WRITE(0,*) inFilters,numFilters
				WRITE(0,*) 'END ERROR'
				STOP 
			END IF		
		
		filtersAv=inFilters
		
		
		!Set file path for each metalcitiy
		allExt(1)%configFile='/1/ext_config.cfg'
		allExt(2)%configFile='/2/ext_config.cfg'
		allExt(3)%configFile='/3/ext_config.cfg'
		allExt(4)%configFile='/4/ext_config.cfg'
		allExt(5)%configFile='/5/ext_config.cfg'
		allExt(6)%configFile='/6/ext_config.cfg'
		allExt(7)%configFile='/7/ext_config.cfg'
		allExt(8)%configFile='/8/ext_config.cfg'
		allExt(9)%configFile='/9/ext_config.cfg'
		allExt(10)%configFile='/10/ext_config.cfg'
		
		allExt%bcArrCoeffs%numCols=numCols_col
		
		!PreInt each met now
		DO i=1,numExt
			CALL preInt(allExt(i)%configFile,filtersAv,allExt(i)%filterConfigPars,allExt(i)%header,allExt(i)%bcs,&
					allExt(i)%bcArrCoeffs,allExt(i)%indexs,allExt(i)%bcIndexSize,rootPathAV)
		END DO
		
		!Pre int spectral typing 
		CALL preIntSpec()
	
	END SUBROUTINE preIntCol
	
	SUBROUTINE getCol(mass,rad,teff,lum,skw,Av,isSingle,colCorrect)
		USE spectral
		IMPLICIT NONE
		DOUBLE PRECISION,DIMENSION(1:2),INTENT(IN) :: mass,rad,teff,lum
		DOUBLE PRECISION,INTENT(IN) :: Av
		INTEGER,DIMENSION(1:2),INTENT(IN) :: skw
		INTEGER,DIMENSION(1:2) :: startype
		DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE,INTENT(OUT) :: colCorrect
		DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE :: col1,col2,zeroCol
		INTEGER,INTENT(IN) :: isSingle
		TYPE(spectralType),DIMENSION(1:2) :: spec
		
		DOUBLE PRECISION :: feh
		INTEGER :: i,aerr,numFilters
		
		numFilters=allExt(1)%bcArrCoeffs%numFilters
		
		ALLOCATE(colCorrect(1:numFilters),col1(1:numFilters),col2(1:numFilters),&
		zeroCol(1:numFilters),STAT=aerr)
			IF (aerr > 0) THEN
				WRITE(0,*) 'ERROR magnitudes.f90 getCol'
				WRITE(0,*) 'ERROR: Memory allocation failed for colours!'
				WRITE(0,*) numFilters
				WRITE(0,*) 'END ERROR'
				STOP
			END IF
		colCorrect=0.d0
		col1=0.d0
		col2=0.d0	
		zeroCol=0.d0

		!Get star type
		CALL getStarType(mass,rad,teff,lum,skw,starType,spec)
		
! 		write(*,*) mass,rad,teff,lum,starType,isSingle		
		if(Av < metExt(1)) THEN
			!Interpolate between 0 and 1
			CALL getColCorrExt(mass,rad,teff,lum,starType,isSingle,allExt(1),col1)
			colCorrect=linearinterpol(av,0.d0,metExt(1),zeroCol,col1)
			RETURN	
		ELSE IF (Av > metExt(numExt)) THEN
			!EXtrapolate above maximum
			!Run at highest
			CALL getColCorrExt(mass,rad,teff,lum,starType,isSingle,allExt(numExt),col1)
			
			colCorrect=(Av/metExt(numExt))*col1
			RETURN
		ELSE
			!Run interpolated bwtween 2 mets
			DO i=1,numExt-1
				if(aV >= metExt(i) .and. feh <= metExt(i+1)) THEN

					CALL getColCorrExt(mass,rad,teff,lum,starType,isSingle,allExt(i),col1)
					CALL getColCorrExt(mass,rad,teff,lum,starType,isSingle,allExt(i+1),col2)			
					!Interpolate over values
					colCorrect=linearInterpol(av,metExt(i),metExt(i+1),col1,col2)

					RETURN
				END IF
			END DO
		END IF
	
	END SUBROUTINE getCol
	
END MODULE AV