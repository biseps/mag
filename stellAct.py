import numpy as np
import scipy.integrate as sci
import warnings as w
w.simplefilter("ignore")
#def mass2bv(mass):
	#return (np.log10(mass)-0.28)/(-0.42)

#def prot(mass,age):
	##Cardini 2007
	##Returns period of rotation in days,age in Gyr
	##valid for m=0.25-1.29
	#return (25.6*(age**0.45)*(1.4-mass))

#def protAigran(mass,age):
	##Aigrain 2004
	#mass=np.atleast_1d(mass)
	#age=np.atleast_1d(age)
	##Cheat and use mass for b-v
	#bv=mass2bv(mass)
	#prot=np.zeros(np.size(mass))	
	#ind0=(bv<0.45)
	##Rates sat at bv<0.45
	#bv[ind0]=0.45	
	#ind1=(0.45<=bv)&(bv<0.62)
	#ind2=(0.62<=bv)
	#prot[ind1]=-0.669+2.580*bv[ind1]+0.5*np.log10(age[ind1]/0.625)
	#prot[ind2]=0.725+0.326*bv[ind2]+0.5*np.log10(age[ind2]/0.625)
	#prot=10**prot
	#return prot
	

#def tauRot(mass):
	##Noyes 1984
	#mass=np.atleast_1d(mass)
	##returns tau Rot in days
	#bv=mass2bv(mass)
	#x=1.0-bv
	#ind=(x>0)
	#tc=np.zeros(np.size(mass))
	#tc[ind]=1.362-0.166*x+0.025*x**2-5.323*x**3
	#tc[np.logical_not(ind)]=1.362-0.14*x
	##Truncates for low mass objects M~0.5msun as now back in radiativly mode
	#tc[bv>1.4]=1.4	
	#return 10**tc

#def rhk(mass,age):
	##noyes 1984
	#return 6.0*10**(-5)*np.exp(-0.9*protAigran(mass,age)/tauRot(mass))

#def sigmaAct(mass,age):
	##returns in ppm
	#r=rhk(mass,age)
	#res=np.zeros(np.size(mass))
	#res[r>0.0]=6.0*10**(10.5+1.75*np.log10(r[r>0.0]))
	#return res

#def sigmaGran(mass,rad,teff):
	##returns in ppm
	#return (75.0*mass**(-0.5)*rad*(teff/5777.0)**0.25)

#def tauGran(mass,rad,teff):
	##returns in s
	#return 220.0*mass**(-1.0)*rad**2*(teff/5777.0)**0.5

#def psd(nu,sigma,tau,transTime):
	#return ((2.0*sigma**2*tau)/(1.0+(2.0*np.pi*nu*tau)**2))
##*np.sinc(nu*transTime)**2

#def psdActGran(nu,sigAct,tauAct,sigGran,tauGran,transTime):
	##return (psd(sigAct,tauAct,nu)+psd(sigGran,tauGran,nu))*(np.sinc(nu*6.5*3600)**2)
	#return (psd(nu,sigAct,tauAct,transTime)+psd(nu,sigGran,tauGran,transTime))

#def solar(mass,rad,teff,age,transTime):
	##gilliand 2011
	#sigAct=sigmaAct(mass,age)
	##Taurot is in days turn to seconds
	#tauAct=tauRot(mass)*3600.0*24.0

	#sigGran=sigmaGran(mass,rad,teff)
	#tauGran1=tauGran(mass,rad,teff)

	#lowLimit=1.0/(3.0*30.4375*3600.0*24.0)
	#upLimit=(1.0/(29.4*60.0))*0.5

	#psdInt=np.zeros([np.size(mass)])
	##for i in range(np.size(mass)):
		##psdInt[i]=sci.romberg(psdActGran,lowLimit,upLimit,(sigAct[i],tauAct[i],sigGran[i],tauGran1[i],transTime[i]),vec_func=True,divmax=15)

	#for i in range(np.size(mass)):
		#psdInt[i]=(sci.romberg(psd,lowLimit,upLimit,(sigAct[i],tauAct[i],transTime[i]),vec_func=True,divmax=10)+
			#sci.romberg(psd,lowLimit,upLimit,(sigGran[i],tauGran1[i],transTime[i]),vec_func=True,divmax=10))
		
	#return np.sqrt(psdInt*lowLimit)*10**-6

#New tatic follow chaplin 2011
lcFreq=1.0/(60.0*30.0)
#in uHz
lcNy=lcFreq*0.5*10**6
#uHz
nuSun=3150.0
tredSun=8907.0
teffSun=5777.0
delT=1550.0

def tred(lum):
	return tredSun*lum**(-0.093)

def nuMax(rad,teff):
	return nuSun*rad**(-2)*(teff/teffSun)

def beta(teff,lum):
	return 1.0-np.exp(-(tred(lum)-teff)/delT)

def neta(rad,teff):
	return np.sinc(0.5*nuMax(rad,teff)/lcNy)

def mass2bv(mass):
	return (np.log10(mass)-0.28)/(-0.42)

def pSolar(mass,rad,teff,lum):
	#returns in ppm**2 uHz
	return 225.0*beta(teff,lum)**(2)*neta(rad,teff)**(2)*rad**(3.5)*(teff/teffSun)**(1.25)

def pGran(rad,teff):
	#returns in ppm**2 uHz
	return 0.1*(nuMax(rad,teff)/nuSun)**(-2)

#Stellar activity defines as per gilliand 2011
def protAigran(mass,age):
	#Aigrain 2004
	mass=np.atleast_1d(mass)
	age=np.atleast_1d(age)
	#Cheat and use mass for b-v
	bv=mass2bv(mass)
	prot=np.zeros(np.size(mass))
	ind0=(bv<0.45)
	#Rates sat at bv<0.45
	bv[ind0]=0.45
	ind1=(0.45<=bv)&(bv<0.62)
	ind2=(0.62<=bv)
	prot[ind1]=-0.669+2.580*bv[ind1]+0.5*np.log10(age[ind1]/0.625)
	prot[ind2]=0.725+0.326*bv[ind2]+0.5*np.log10(age[ind2]/0.625)
	prot=10**prot
	return prot


def tauRot(mass):
	#Noyes 1984
	mass=np.atleast_1d(mass)
	#returns tau Rot in days
	bv=mass2bv(mass)
	x=1.0-bv
	ind=(x>0)
	tc=np.zeros(np.size(mass))
	tc[ind]=1.362-0.166*x+0.025*x**2-5.323*x**3
	tc[np.logical_not(ind)]=1.362-0.14*x
	#Truncates for low mass objects M~0.5msun as now back in radiativly mode
	tc[bv>1.4]=1.4
	return 10**tc

def rhk(mass,age):
	#noyes 1984
	return 6.0*10**(-5)*np.exp(-0.9*protAigran(mass,age)/tauRot(mass))

def sigmaAct(mass,age):
	#returns in ppm
	r=rhk(mass,age)
	res=np.zeros(np.size(mass))
	res[r>0.0]=6.0*10**(10.5+1.75*np.log10(r[r>0.0]))
	return res

def psd(nu,sig,tau):
	return 2.0*sig**(2)*tau/(1.0+(2.0*np.pi*nu*tau)**(2))

def pAct(mass,rad,teff,age):
	sig=sigmaAct(mass,age)
	tau=8.0*3600.0*24.0

	intFreq=np.zeros(np.size(sig))
	for i in range(0,len(sig)):
		intFreq[i]=sci.romberg(psd,0.13,555.0,args=(sig[i],tau),divmax=15,rtol=10**-10,vec_func=True)
	
	return intFreq

def solar(mass,rad,teff,lum,age):
	return np.sqrt((pGran(rad,teff)+pSolar(mass,rad,teff,lum)+pAct(mass,rad,teff,age))*nuMax(rad,teff))*10**(-6)



#GB,Che and AGB fro hekker 2012 in absence of other data we pick random mu max
def gb(mass):
	#in microHz
	minV=1.0
	maxV=200.0
	#Pick numbers linearly from log space
	res=10**(np.random.uniform(np.log10(minV),np.log10(maxV),size=np.size(mass)))
	res=(3.5*res**(-1.28)+1.4*res**(-1.5))*10**7
	
	return np.sqrt(res)*10**-6

	
def che(mass):
	#aka RC stars
	#in microHz
	minV=20.0
	maxV=40.0
	#Pick numbers linearly from log space
	res=10**(np.random.uniform(np.log10(minV),np.log10(maxV),size=np.size(mass)))
	res=(3.5*res**(-1.28)+1.4*res**(-1.5))*10**7
	
	return np.sqrt(res)*10**-6

def agb(mass):
	#in microHz
	minV=1.0
	maxV=20.0
	#Pick numbers linearly from log space
	res=10**(np.random.uniform(np.log10(minV),np.log10(maxV),size=np.size(mass)))
	res=(3.5*res**(-1.28)+1.4*res**(-1.5))*10**7
	
	return np.sqrt(res)*10**-6

#Random guassian centered on 1% with sigma of 0.5%, clipped such that the min varaince is 0.5%
def wd(mass):
	ran=np.random.randn(np.size(mass))*0.5+1.0
	ran[ran<0.5]=0.5
	return ran/100.0

def stellOscill(mass,rad,teff,lum,age,sType):
	#sTypes 0,1 MS 2-9 GB/CHe/AGB/nHe, 10-12 WD, 13 NS

	res=np.zeros(np.size(mass))

	#Zero noise for WD
	indWD=((sType>=10)&(sType<=12))
	res[indWD]=wd(mass)
	#Zero noise for NS
	indNS=(sType==13)
	res[indNS]=wd(indNS)

	
	#For solar type stars
	indMS=((sType==0)|(sType==1))
	res[indMS]=solar(mass[indMS],rad[indMS],teff[indMS],lum[indMS],age[indMS])

	#HG+GB
	indGB=((sType==2)|(sType==3))
	res[indGB]=gb(mass[indGB])
		
	#CHe
	indCHe=(sType==4)
	res[indCHe]=che(mass[indCHe])

	#AGB+TPAGB?
	indAGB=((sType==5)|(sType==6))
	res[indAGB]=agb(mass[indAGB])
		
	return res
