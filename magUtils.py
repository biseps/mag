import numpy as np
import scipy as sci
import scipy.interpolate as inter
import csv
from sets import Set
import os
import glob
import string as st
import bisect as bisect


#root="/home/Rob/Desktop/biseps/tables/"
root="/padata/beta/users/rfarmer/tables/"

# girardi
# bd dusty atmos
rootBC=root+"BC3/"
rootAV=root+"extinct/"

# Claret & blowman tables
# teff and logg and pull out coeff
# average over metalicity or av
rootGD=root+"grav2/"
rootLDC=root+"ldc2/"

metFolder=["m25","m20","m15","m10","m05","p00","p05"]
metVal=[-2.5,-2.0,-1.5,-1.0,-0.5,0.0,0.5]

extFolder=["1","2","3","4","5","6","7","8","9","10"]
extVal=[1,2,3,4,5,6,7,8,9,10]

gdFolder=["m5.0","m4.5","m4.0","m3.5","m3.0","m2.5","m2.0","m1.5","m1.0","m0.5","m0.3","m0.2","m0.1","p0.0","p0.1","p0.2","p0.3","p0.5","p0.0"]

gdVal=[-5.0,-4.5,-4.0,-3.5,-3.0,-2.5,-2.0,-1.5,-1.0,-0.5,-0.3,-0.2,-0.1,0.0,0.1,0.2,0.3,0.5,1.0]

baseHeadname="_header_"

loadedFiles=[]
loadedData=[]

#Sun
teffSun=5777.0
loggSun=4.44
zSun=0.02
kwSun=1
metSun=0.0
vSun=-26.76
alpha=0.0
lumSun=1.0
#Defined as vSun+31.572+BC_v,sun derive BC from tables and use that
boloMagSun=4.75

class _emptyClass():
	pass

class loadFiles(_emptyClass):
	def __init__(self,root,cType,filters):
		self.dat=[]
		self.filters=filters

		if cType=="bc":
			self.folder=metFolder
			self.values=metVal
		elif cType=="ext":
			self.folder=extFolder
			self.values=extVal
		elif cType=="gd":
			self.folder=gdFolder
			self.values=gdVal
		elif cType=="ldc1":
			self.folder=gdFolder
			self.values=gdVal
		elif cType=="ldc2":
			self.folder=gdFolder
			self.values=gdVal
		else:
			print "Not valid cType"
			exit()

		#Scan the folder given and load the filenames of all filters/mets/types avaible
		for  i in xrange(len(self.folder)):
			self.dat.append(_emptyClass())
			self.dat[i].val=self.values[i]
			self.dat[i].folder=root+self.folder[i]+"/"

			#Grab all the filter files
			self.dat[i].files=self._listAllFiles(self.dat[i].folder+cType)

			#Drop any file thats not part of filter list
			self.dat[i].files=[x for x in self.dat[i].files if self._filterType(x) in filters]

			#Read in the header file for each met
			self.dat[i].head=[]
			j=0
			for inFile in self._listAllFiles(self.dat[i].folder+cType+baseHeadname):
				self.dat[i].head.append(_emptyClass())
				#Read the file
				self.dat[i].head[j]=self._readHeader(inFile)
				#Extract the type ie bc_header_main.dat returns main
				self.dat[i].head[j].hType=self._headType(inFile)
				#print self.dat[i].head[j].hType
				j=j+1

			#Load up the data for the filters
			self.dat[i].cfg=[]
			j=0
			for inFile in self.dat[i].files:
				self.dat[i].cfg.append(_emptyClass())
				self.dat[i].cfg[j].file=inFile
				self.dat[i].cfg[j].dat=self._readData(inFile)
				self.dat[i].cfg[j].hType=self._headType(inFile)
				self.dat[i].cfg[j].val=self._type(inFile,self.folder,self.values)
				self.dat[i].cfg[j].filt=self._filterType(inFile)
				j=j+1

				
	def read_array(self,filename,dtype,sep=None):
		"""
		Read a file with an arbitrary number of columns.
		The type of data in each column is arbitrary
		It will be cast to the given dtype at runtime
		"""
		#cast = np.cast
		#size=len(dtype)
		#data = [[] for dummy in xrange(size)]
		#with open(filename,'r') as f:
			#for line in f:
				#fields = line.strip().split(sep)
				#for i, number in enumerate(fields):
					#if len(number) == 0 or number=='\n':
						#number=0
					#data[i].append(number)
		#for i in xrange(size):
			#data[i] = cast[dtype[i]](data[i])
		#return np.rec.array(data, dtype=dtype)
	
		return np.loadtxt(filename,unpack=True)
	
	
	def _readConfig(self,file):
		#See if file exists in cache if not use the exception thrown by index to swicth to loading the data
		try:
			ind=loadedFiles.index(file)
			return loadedData[ind]
		except:
			e=_emptyClass()
			e.filt,e.sys,e.fileLines,e.files=np.loadtxt(file,unpack=True)
			loadedFiles.append(file)
			loadedData.append(e)
		#return self.read_array(cFile,np.dtype([('filt','int'),('sys','int'),('fileLines','int'),('files','a65')]))
			return e

	def _readHeader(self,file):
		try:
			ind=loadedFiles.index(file)
			return loadedData[ind]
		except:
			e=_emptyClass()
			e.teff,e.logg=np.loadtxt(file,unpack=True)
			loadedFiles.append(file)
			loadedData.append(e)
		#return self.read_array(hFile,np.dtype([('teff','f8'),('logg','f8')]))
			return e

	def _readData(self,file):
		try:
			ind=loadedFiles.index(file)
			return loadedData[ind]
		except:
			e=_emptyClass()
			e.c1=np.loadtxt(file,unpack=True)
			loadedFiles.append(file)
			loadedData.append(e)
		#return self.read_array(file,np.dtype([('c1','f8')]))
			return e

	def _listAllFiles(self,folder):
		"""
		Makes a list of files in a folder without including and ~ files
		"""
		l=[]
		for inFile in glob.glob(folder+'*'):
			#Drop file if we have a ~ in it
			if "~" in inFile:
				continue
			l.append(inFile)
		return l

	def _headType(self,file):
		#Extract the type ie bc_header_main.dat returns main
		return st.split(st.split(os.path.basename(file),"_")[2],".")[0]
	
	def _filterType(self,file):
		#Extract the filter type ie bc_100_main.dat returns 100
		return st.split(os.path.basename(file),"_")[1]

	def _type(self,file,folder,values):
		#Extract the folder type ie BC/p00/bc_100_main.dat returns 0.0 the
		#correspodning value from the value array ie p00 => 0.0
		for x in st.split(file,"/"):
			if x in folder:
				return values[folder.index(x)]

class utils(_emptyClass):
	def __init__(self):
		pass

	def z2met(self,z):
		return np.log10(float(z)/zSun)+alpha

	def calcType(self,kw,teff):
		#Yes everything  isnt a main sequence object but main contains everything other than m Giants
		#and acts as a fallback classification
		hType="main"
		#Ref http://adsabs.harvard.edu/abs/1996AJ....111.1705D
		if ((kw==2) or (kw==3)) and (teff<3800.0):
			hType="mGiants"
		return hType

	def getCoeffs2d(self,teff,logg,teffRef,loggRef,coeff):

		#Dont extrapolate when outside teff range just use nearest value
		if teff < teffRef[0] or teff > teffRef[-1]:
			res=inter.griddata((teffRef,loggRef),coeff,(teff,logg),method='nearest')
		else:
			#We want to find the reference teff values just before and just after ie if teff=5777 then we want 5500 and 6000
			ind=(teffRef>=teffRef[np.searchsorted(teffRef,teff,side="left")-1])&(teffRef<=teffRef[np.searchsorted(teffRef,teff,side="right")])
			#Fallback if we dont have enough points to nearest neigbour
			if (np.all(logg<loggRef[ind]) or np.all(logg>loggRef[ind])) or np.count_nonzero(ind) < 5:
				res=inter.griddata((teffRef,loggRef),coeff,(teff,logg),method='nearest')
			else:
				res=inter.griddata((teffRef[ind],loggRef[ind]),coeff[ind],(teff,logg),method='linear')

		#And if eveythting has failed fall back to nearest neighbour
		if np.isnan(res):
			res=inter.griddata((teffRef,loggRef),coeff,(teff,logg),method='nearest')

		return res

	def getCoeffs1d(self,teff,teffRef,coeff):
		#Switch to only interpolating over 1d data as logg is defined
		#as 0 for mGiants in Girardi's work
		# and we cant interpolate with griddata over a column (logg) that has all the same values
		if teff < teffRef.min():
			res=coeff[np.argmin(teffRef)]
		elif teff > teffRef.max():
			res=coeff[np.argmax(teffRef)]
		else:
			res=inter.griddata((teffRef),coeff,(teff),method='linear')
		return res

	def calcValType(self,val,values):
	#Returns the index(s) of the val from a list of values, and setting the nearest value if we exceed the list
		if val in values:
			return [values.index(val)]
		if val <= values[0]:
			return [0]
		if val >= values[-1]:
			return [-1]
		for i in xrange(len(values)):
			if i==len(values):
				continue
			if values[i] < val < values[i+1]:
				return [i,i+1]


	def interVal(self,init,teff,logg,val,kw):
		#Pass in a instance of loadFiles plus the teff, logg, and VALUE that we deriving for (metallicity,av etc)
		#and the star type 
		#and returns a list of coeffcients in order of the filters
		valCheck=self.calcValType(val,init.values)
		sysCheck=self.calcType(kw,teff)

		#In case of us laying exactly on a boundary in VALUES arrays
		if len(valCheck)==1:
			valInd=valCheck[0]
			#Get header data
			teffRef,loggRef=self._getHeaders(init,valInd,sysCheck)
			res=self.getCoeffs(init,valCheck[0],sysCheck,teff,logg,val,kw,init.filters,teffRef,loggRef)


		if len(valCheck)==2:
			res=[]
			#Grab coeff one
			valInd=valCheck[0]
			#Get header data
			teffRef,loggRef=self._getHeaders(init,valInd,sysCheck)
					
			res1=self.getCoeffs(init,valCheck[0],sysCheck,teff,logg,val,kw,init.filters,teffRef,loggRef)

			#Grab coeff two may have diff header to coeff 1
			valInd=valCheck[1]
			teffRef,loggRef=self._getHeaders(init,valInd,sysCheck)
					
			res2=self.getCoeffs(init,valCheck[1],sysCheck,teff,logg,val,kw,init.filters,teffRef,loggRef)

			#linearly interpolate between the two coefficents
			for i in xrange(len(res1)):
				res.append(inter.griddata(np.array([init.values[valCheck[0]],
					init.values[valCheck[1]]]),np.array([res1[i],res2[i]]),
					val,method='linear'))

		return res


	def getCoeffs(self,init,valInd,sysCheck,teff,logg,val,kw,filters,teffRef,loggRef):
		#Extract the header data we need for this star
		#for k in xrange(len(init.dat[valInd].head)):
			#if init.dat[valInd].head[k].hType==sysCheck:
				#teffRef=init.dat[valInd].head[k].teff
				#loggRef=init.dat[valInd].head[k].logg

		res=[]
		for i in xrange(len(filters)):
			tmp=np.nan
			for j in xrange(len(init.dat[valInd].cfg)):
				if init.dat[valInd].cfg[j].filt==filters[i] and init.dat[valInd].cfg[j].hType==sysCheck:
					if sysCheck == "mGiants":
						#Cant handle interpolating over 1d grid
						tmp=self.getCoeffs1d(teff,teffRef,init.dat[valInd].cfg[j].dat.c1)	
					else:
						tmp=self.getCoeffs2d(teff,logg,teffRef,loggRef,init.dat[valInd].cfg[j].dat.c1)
						
			if np.isnan(tmp):
			#Maybe we dont have the type defined thus fallback on main
				sysCheck="main"
				for j in xrange(len(init.dat[valInd].cfg)):
					if init.dat[valInd].cfg[j].filt==filters[i] and init.dat[valInd].cfg[j].hType==sysCheck:
						tmp=self.getCoeffs2d(teff,logg,teffRef,loggRef,init.dat[valInd].cfg[j].dat.c1)
			res.append(tmp)
		return res

	def _getHeaders(self,init,valInd,sysCheck):
		teffRef=[np.nan]
		loggRef=[np.nan]
		for k in xrange(len(init.dat[valInd].head)):
				if init.dat[valInd].head[k].hType==sysCheck:
					teffRef=init.dat[valInd].head[k].teff
					loggRef=init.dat[valInd].head[k].logg

		#Fallback on main if header data not set
		if np.any(np.isnan(teffRef)) or np.any(np.isnan(loggRef)):
				for k in xrange(len(init.dat[valInd].head)):
					if init.dat[valInd].head[k].hType=='main':
						teffRef=init.dat[valInd].head[k].teff
						loggRef=init.dat[valInd].head[k].logg

		return teffRef,loggRef

class Memoize:
    """Memoize(fn) - an instance which acts like fn but memoizes its arguments
       Will only work on functions with non-mutable arguments

	   Use as 
		func = Memoize(func)
    """
    def __init__(self, fn):
        self.fn = fn
        self.memo = {}
    def __call__(self, *args):
        if not self.memo.has_key(args):
            self.memo[args] = self.fn(*args)
        return self.memo[args]
	

class calcBC(_emptyClass,loadFiles,utils):
	def __init__(self,filters):
		#Load data
		self.init=loadFiles(rootBC,'bc',filters)
		#self.utils=utils()
		#Calc suns mags in passbands
		self.sunBCs=self.bc2(teffSun,loggSun,zSun,kwSun)
		self.sunMags=self.calcMag(lumSun,self.sunBCs)

		self.bc=Memoize(self.bc2)
		#self.bc=self.bc2
		
	def calcMag(self,lum,bcs):
		#print bcs
		res=[]
		m=-2.5*np.log10(lum)+boloMagSun
		for i in bcs:
			res.append(m-i)
		return res

	def bc2(self,teff,logg,met,kw):
		u=utils()
		return self.interVal(self.init,teff,logg,u.z2met(met),kw)

	def magBin(self,lum1,lum2,bcs1,bcs2,isSingle):
		#If the systems is a single star then return mag1
		#else in binary add the fluxes and return m1+m2
		if isSingle:
			return self.calcMag(lum1,bcs1)
		else:
			m1=self.calcMag(lum1,bcs1)
			m2=self.calcMag(lum2,bcs2)
			#This is luminosity in a passband not total lum
			res=[]
			for i in range(len(m1)):
				l1=10**(-(m1[i]-self.sunMags[i])/2.5)
				l2=10**(-(m2[i]-self.sunMags[i])/2.5)
				res.append(self.sunMags[i]-2.5*np.log10(l1+l2))
			return res
			
class calcAV(_emptyClass,loadFiles,utils):
	def __init__(self,filters):
		#Load data
		self.init=loadFiles(rootAV,'ext',filters)

		self.av=Memoize(self.av2)

	def av2(self,teff,logg,av,kw):
		tmp=self.interVal(self.init,teff,logg,av,kw)
		res=[]
#		for i in tmp:
#			res.append(av*i)
		return tmp

	def avBin(self,t1,t2,l1,l2,logg1,logg2,kw1,kw2,av,isSingle):
		av1=self.av(t1,logg1,av,kw1)
		if isSingle:
			return av1
		else:
                    # same as bolometric correct
                    # different for different wavelengths
			av2=self.av2(t2,logg2,av,kw2)
			res=[]
			for i in range(len(av1)):
                            # average over both for binary
				res.append((av1[i]*l1+av2[i]*l2)/(l1+l2))
			#return av/res
			return res			

	def distCorrect(self,mag,dis,ext,av):
		for i in range(len(mag)):
			mag[i]=mag[i]+5.0*np.log10(dis)-5.0+ext[i]*av
		return mag				

class calcGD(_emptyClass,loadFiles,utils):
	def __init__(self,filters):
		#Load data
		self.init=loadFiles(rootGD,'gd',filters)
		self.gd=Memoize(self.gd2)


	def gd2(self,teff,logg,met,kw):
		u=utils()
		return self.interVal(self.init,teff,logg,u.z2met(met),kw)

class calcLDC(_emptyClass,loadFiles,utils):
	def __init__(self,filters):
		#Load data
		self.init1=loadFiles(rootLDC,'ldc1',filters)
		self.init2=loadFiles(rootLDC,'ldc2',filters)
		self.ldc=Memoize(self.ldc2)

	def ldc2(self,teff,logg,met,kw):
		u=utils()
		l1=self.interVal(self.init1,teff,logg,u.z2met(met),kw)
		l2=self.interVal(self.init2,teff,logg,u.z2met(met),kw)
		return l1,l2


########################
# Examples on how to use
########################

#filters=['500','310']
##init=loadFiles(rootBC,'bc',filters)
#x=calcBC(filters)
#y=x.bc(5000.0,4.0,0.02,0)
#print x.calcMag(1.0,y)

#print "*"

#x=calcAV(filters)
#y=x.av(5000.0,4.0,1.0,0)
#print y

#print "*"
#x=calcGD(filters)
#y=x.gd(5000.0,4.0,0.02,0)
#print y

#print "*"
#x=calcLDC(filters)
#y=x.ldc(5000.0,4.0,0.02,0)
#print y

#######################
