PROGRAM testMag
	USE av

	IMPLICIT NONE
	DOUBLE PRECISION,ALLOCATABLE,DIMENSION(:) :: colCorr

	CALL preIntCol((/'500'/))

	CALL getCol((/0.36d0,2.d0/),(/1.d0,2.d0/),(/5500.d0,6000.d0/),(/(1.d0**2)*(5500.0/5777.d0)**4,(2.d0**2)*(6000.0/5777.d0)**4/),&
	(/1,1/),1.0d0,0,colCorr)

	write(*,*) colCorr

END PROGRAM testMag