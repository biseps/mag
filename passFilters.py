#Turns a filter name into a number suitable for use as BC name

def passFilters(names):
	out=[]
	for i in names:
		i=i.lower()
		n=0
		if i == "kepler" or i == "kp":
			n=500
		if i == "d51":
			n=505
		#sloan
		if i == "sloan u":
			n=310
		if i == "sloan g":
			n=311
		if i == "sloan r":
			n=312
		if i == "sloan i":
			n=313
		if i == "sloan z":
			n=314
			
		#Johnson-cousins-glass
		if i == "u":
			n=350
		if i == "b":
			n=351
		if i == "v":
			n=352
		if i == "r":
			n=353
		if i == "i":
			n=354
		if i == "j":
			n=355
		if i == "h":
			n=356
		if i == "k":
			n=357
			
		#stroemgren V        u        v        b        y     Hb_w     Hb_n
		if i == "stroem v":
			n=330
		if i == "stroem u":
			n=331
		if i == "stroem b":
			n=332
		if i == "stroem y":
			n=333
		if i == "stroem hb_w":
			n=334
		if i == "stroem hb_n":
			n=335
			
		#2mass J        H       Ks
		if i == "2mass j":
			n=100
		if i == "2mass h":
			n=101
		if i == "2mass ks":
			n=102

		#ogle  U        B        V        I
		if i == "ogle u":
			n=300
		if i == "ogle b":
			n=301
		if i == "ogle v":
			n=302
		if i == "ogle i":
			n=303
			
		#spitzer IRAC_3.6 IRAC_4.5 IRAC_5.8 IRAC_8.0  MIPS_24  MIPS_70 MIPS_160
		if i == "spitzer 3.6":
			n=320	
		if i == "spitzer 4.5":
			n=321			
		if i == "spitzer 5.8":
			n=322			
		if i == "spitzer 8.0":
			n=323			
		if i == "spitzer 24":
			n=324			
		if i == "spitzer 70":
			n=325			
		if i == "spitzer 160":
			n=326
			
		# ukidss Z        Y        J        H        K
		if i == "ukidss z":
			n=360			
		if i == "ukidss y":
			n=361			
		if i == "ukidss j":
			n=362			
		if i == "ukidss h":
			n=363			
		if i == "ukidss k":
			n=364			
			
		if n==0:
			print "Error filter not known"
			exit()
			
		out.append(str(n))
	return out