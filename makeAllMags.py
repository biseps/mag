
# Translates the ABSOLUTE mags calculated in "absMag.py"
# and the distance found in "ranLoc.f90" 
# to create APPARENT mag in specified filter bands

import biseps as b
import numpy as np
import magUtils as m
import getopt
import sys as sys
import passFilters as p

opts, extra = getopt.getopt(sys.argv[1:],'')
#extra=["extract.1","ranLoc.dat.1","1"]	
kepFile=str(extra[0])
ranLocFile=str(extra[1])
isSingle=int(extra[2])


x=b.readFile()
	 
x.kepler5(kepFile)
x.ranLoc(ranLocFile)

filtIn=['sloan u','sloan g','sloan r','sloan i','sloan z','d51','j','h','k']
filters=p.passFilters(filtIn)
#filters=['310','311','312','313','314','505','355','356','357']
bb=m.calcBC(filters)
av=m.calcAV(filters)

# Use "len" to count number of elements
# (it uses less memory?)
# kSize=np.count_nonzero(x.kep.m1)
# print "before assigning ksize"
kSize=len(x.kep.m1)
# print kSize
# print "after assigning ksize"

for i in range(kSize):
	bc1=bb.bc(x.kep[i].t1,x.kep.logg1[i],x.kep.met[i],x.kep.kw1[i])
	bc2=bb.bc(x.kep[i].t2,x.kep.logg2[i],x.kep.met[i],x.kep.kw2[i])
		
	mB=bb.magBin(x.kep[i].l1,x.kep[i].l2,bc1,bc2,isSingle)

	#We turn ext/0.88 ext is extinction in kepler not extinction V band so turn intno V band ext
	ext=av.avBin(x.kep[i].t1,x.kep[i].t2,x.kep[i].l1,x.kep[i].l2,x.kep.logg1[i],x.kep.logg2[i],x.kep[i].kw1,x.kep[i].kw2,x.ranLoc[i].ext/0.88,isSingle)

	mag=av.distCorrect(mB,x.ranLoc[i].dist,ext,x.ranLoc[i].ext/0.88)

	if mag[2]-mag[3] > 0.3:
		kp=0.7*mag[4]+0.3*mag[2]
	if mag[1]-mag[2] > 0.3:
		kp=0.7*mag[3]+0.3*mag[1]
	else:
		kp=0.75*mag[2]+0.25*mag[1]

	print x.kep[i].id,x.ranLoc[i].long,x.ranLoc[i].lat,' '.join([str(item) for item in mag]),kp

#print >> stderr x.kep[-1].id
