import biseps as b
import numpy as np
import magUtils as m
import getopt
import sys as sys

#for i in sing;do for j in targ targAll;do python /padata/beta/users/rfarmer/code/BiSEPSv3.0/mag/makeKPMag.py extract."$i"."$j".1 ranLoc."$i"."$j".1 1 > kp."$i"."$j".1 ;done;done

#for i in bin;do for j in targ targAll;do python /padata/beta/users/rfarmer/code/BiSEPSv3.0/mag/makeKPMag.py extract."$i"."$j".1 ranLoc."$i"."$j".1 0 > kp."$i"."$j".1 ;done;done



opts, extra = getopt.getopt(sys.argv[1:],'')
#extra=["extract.1","ranLoc.dat.1","1"]	
kepFile=str(extra[0])
ranLocFile=str(extra[1])
isSingle=int(extra[2])


x=b.readFile()
	 
x.kepler5(kepFile)
x.ranLoc(ranLocFile)

filters=['310','311','312','313','314','505','355','356','357']
bb=m.calcBC(filters)
av=m.calcAV(filters)
kSize=np.count_nonzero(x.kep.m1)

filters=['500']
bb=m.calcBC(filters)
av=m.calcAV(filters)
kSize=np.count_nonzero(x.kep.m1)

for i in range(kSize):
	bc1=bb.bc(x.kep[i].t1,x.kep.logg1[i],x.kep.met[i],x.kep.kw1[i])
	bc2=bb.bc(x.kep[i].t2,x.kep.logg2[i],x.kep.met[i],x.kep.kw2[i])
		
	mB=bb.magBin(x.kep[i].l1,x.kep[i].l2,bc1,bc2,isSingle)

	#We turn ext/0.88 ext is extinction in kepler not extinction V band so turn intno V band ext
	ext=av.avBin(x.kep[i].t1,x.kep[i].t2,x.kep[i].l1,x.kep[i].l2,x.kep.logg1[i],x.kep.logg2[i],x.kep[i].kw1,x.kep[i].kw2,x.ranLoc[i].ext/0.88,isSingle)

	mag=av.distCorrect(mB,x.ranLoc[i].dist,ext,x.ranLoc[i].ext/0.88)

	print x.kep[i].id,' '.join([str(item) for item in mag])

#print >> stderr, x.kep[-1].id