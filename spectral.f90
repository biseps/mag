MODULE spectral
	USE magUtils
	IMPLICIT NONE
	
	TYPE :: spectralType
		CHARACTER(len=4) :: class,subClass,Lumin
	END TYPE spectralType
	
	CHARACTER(len=maxFilePath) :: configLum='/padata/beta/users/rfarmer/tables/specType/loggLum.csv'
	CHARACTER(len=maxFilePath) :: configTeff='/padata/beta/users/rfarmer/tables/specType/loggTeff.csv'
	
	INTEGER :: TeffFileSize,LumFileSize

	INTEGER,PARAMETER :: numSpec=8
	DOUBLE PRECISION,ALLOCATABLE,DIMENSION(:,:) :: specTeff,specLum
	TYPE(spectralType),ALLOCATABLE,DIMENSION(:) :: specTypeTeff,specTypeLum
	CHARACTER(len=3),DIMENSION(1:numSpec) :: lumClass=(/"Ia+","Ia ","Iab","Ib ","II ","III","IV ","V  "/)

	REAL,ALLOCATABLE,DIMENSION(:) :: specTeffSVar,specLumSVar
  	
	INTEGER :: isInt=0
  	
	CONTAINS
	
	SUBROUTINE preIntSpec()
		IMPLICIT NONE
		INTEGER :: i,j,aerr
		
		IF(isInt==1) RETURN
	
		!Spectral typing
		LumfileSize=getFileSize(configLum)
		
		TefffileSize=getFileSize(configTeff)
		
		ALLOCATE(specTeff(1:TefffileSize,1:numSpec),specLum(1:LumfileSize,1:numSpec),&
		specTypeTeff(1:TefffileSize),specTypeLum(1:LumfileSize),&
		specLumSVar(1:LumfileSize),specTeffSVar(1:TefffileSize),STAT=aerr)
			IF (aerr > 0) THEN
				WRITE(0,*) 'ERROR magnitudes.f90 preIntSpec'
				WRITE(0,*) '1, ERROR ALLOCATING MEMORY! spectral Teff'
				WRITE(0,*) 'END ERROR'
				STOP 
			END IF
			
		OPEN(1,FILE=configLum(1:LEN_TRIM(configLum)),STATUS='OLD',ACTION='READ')
		DO i=1,LumfileSize
			READ(1,*) specTypeLum(i)%class,specTypeLum(i)%subClass,specLumSVar(i),(specLum(i,j), j=1,numSpec)
		END DO
		CLOSE(1)

		OPEN(1,FILE=configTeff(1:LEN_TRIM(configTeff)),STATUS='OLD',ACTION='READ')
		DO i=1,TefffileSize
			READ(1,*) specTypeTeff(i)%class,specTypeTeff(i)%subClass,specTeffSVar(i),(specTeff(i,j), j=1,numSpec)
		END DO
		CLOSE(1)
		
		isInt=1

	END SUBROUTINE preIntSpec
	
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Gets spectral type from the logg teff and logg lum from
! the tables in
! http://adsabs.harvard.edu/abs/1987A%26A...177..217D
! de Jager et al 1987
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	SUBROUTINE getSpecType(teff,lum,skw,specType)
		IMPLICIT NONE
		DOUBLE PRECISION,INTENT(IN) :: teff,lum
		TYPE(spectralType),INTENT(OUT) :: specType
		INTEGER,INTENT(IN) :: skw
		INTEGER :: j
		INTEGER :: minIndTeff,minIndLum,tmpG
		INTEGER,DIMENSION(1:7) :: minIndTeffG,minIndLumG
		DOUBLE PRECISION,DIMENSION(1:7) :: newMin
		DOUBLE PRECISION :: logteff,loglum


		specType%class="   "
		specType%subClass="   "
		specType%Lumin="   "

		logteff=log10(teff)
		loglum=log10(lum)

	! 	loggteff=log10(5777.0)
	!  	logglum=log10(1.0)
	! 	s%kw=1


		SELECT CASE(skw)
		CASE(0:1)
			!Star is MS so set luminsoity class to V, only need the Teff table for closest match
			
			specType%Lumin="V  "
			minIndTeff=MINLOC((abs(specTeff(:,8)-logteff)),DIM=1)

			specType%class=specTypeTeff(minIndTeff)%class

			specType%subclass=specTypeTeff(minIndTeff)%subclass
		CASE(2:9)

			minIndTeffG=MINLOC((abs(specTeff(:,1:7)-logteff)),DIM=1)
			minIndLumG=MINLOC((abs(specLum(:,1:7)-logLum)),DIM=1)

			DO j=1,7
				newMin(j)=(ABS(specTeff(minIndTeffG(j),j)-logTeff)/(0.5*(specTeff(minIndTeffG(j),j)+logTeff)))+&
				(ABS(specLum(minIndLumG(j),j)-logLum)/(0.5*(specTeff(minIndLumG(j),j)+logLum)))
			END DO

			tmpG=MINLOC(newMin,DIM=1)
			specType%Lumin=lumClass(tmpG)
			specType%class=specTypeTeff(minIndTeffG(tmpG))%class
			specType%subClass=specTypeTeff(minIndTeffG(tmpG))%subClass
		
		CASE(10:12)
			specType%class="WD "
		CASE(13)
			specType%class="NS "
		CASE(14)
			specType%class="BH "
		CASE(15)
			specType%class="mSN"
		END SELECT


	END SUBROUTINE
	
	SUBROUTINE getStarType(mass,rad,teff,lum,skw,starType,spec)
		IMPLICIT NONE
		DOUBLE PRECISION,DIMENSION(1:2),INTENT(IN) :: mass,rad,teff,lum
		INTEGER,DIMENSION(1:2),INTENT(OUT) :: startype
		INTEGER,DIMENSION(1:2),INTENT(IN) :: skw
		TYPE(spectralType),DIMENSION(1:2),INTENT(INOUT) :: spec
		INTEGER :: i
		
		!Take what we now about the stars get their spectral type and
		!place into each starType
		!Where starType=
		!1 for All stars and is default
		!2 is for M gaints
		!3 for WD's
		
		DO i=1,2
			CALL getSpecType(teff(i),lum(i),skw(i),spec(i))	
		
			starType(i)=1
			
			IF(spec(i)%class=='M' .and. spec(i)%lumin/=lumClass(8)) starType(i)=2
! 			IF(spec(i)%class=='WD') starType(i)=3
		END DO
		
	END SUBROUTINE getStarType
	
	
	
END MODULE spectral
