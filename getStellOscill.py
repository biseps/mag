import numpy as np
import biseps as b
import getopt
import sys as sys
import stellAct as sos

opts, extra = getopt.getopt(sys.argv[1:],'')

version=extra[0]
isSingle=extra[1]

#we also need to load up the correponding files from the other runs in pop_{b,s}_{y,o} to get the stell oscill data
x=b.readFile()

x.kepler5('extract.'+version)

age=np.random.uniform(x.kep.birth,x.kep.death)/1000.0

sy1=sos.stellOscill(x.kep.m1,x.kep.r1,x.kep.t1,x.kep.l1,age,x.kep.kw1)
if isSingle=="1":
	stellBG=sy1
else:
	ll1=x.kep.l1/(x.kep.l1+x.kep.l2)
	ll2=x.kep.l2/(x.kep.l1+x.kep.l2)
	sy2=sos.stellOscill(x.kep.m2,x.kep.r2,x.kep.t2,x.kep.l2,age,x.kep.kw2)
	stellBG=np.sqrt((ll1*sy1)**2+(ll2*sy2)**2)

output=[x.kep.id,x.kep.num,stellBG]
output=np.column_stack([x.flatten() for x in output])

np.savetxt('stellOscill.'+version,output,fmt="%d %.5f %.10f")
