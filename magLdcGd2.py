import biseps as b
import numpy as np
import magUtils as m
import getopt
import sys as sys

opts, extra = getopt.getopt(sys.argv[1:],'')
file=str(extra[0])
sysType='targ'
#Iterates over a .extract file generating the quadratic LDC and GD coeffeicent for the binaries
#in the Kp filter band



dtype=np.dtype([('num','float'),('m1','float'),('r1','float'),('t1','float'),('l1','float'),
       ('m2','float'),('r2','float'),('t2','float'),('l2','float'),('p','float'),('birth','float'),('death','float'),
('massTran','int'),('kw1','int'),('kw2','int'),('bw','int'),('met','float'),('evol','a100')])

ex=b.bfuncs()
	  
filters=['500']
	
l=m.calcLDC(filters)
g=m.calcGD(filters)
bb=m.calcBC(filters)

cast = np.cast
size=len(dtype)
sep=" "
with open(file,'r') as f:
	for line in f:
		x = [[] for dummy in xrange(size)]
		fields = line.strip().split(sep)
		for i, number in enumerate(fields):
			if len(number) == 0 or number=='\n' or number=="#":
				number=0
			x[i].append(number)
		for i in xrange(size):
			x[i] = cast[dtype[i]](x[i])
		x=np.rec.array(x, dtype=dtype)
		
		logg1=ex.massrad2logg(x.m1,x.r1)
		logg2=ex.massrad2logg(x.m2,x.r2)
		ldc11,ldc12=l.ldc(x.t1,logg1,x.met,x.kw1)
		ldc21,ldc22=l.ldc(x.t2,logg2,x.met,x.kw2)
		gd1=g.gd(x.t1,logg1,x.met,x.kw1)
		gd2=g.gd(x.t2,logg2,x.met,x.kw2)
		bc1=bb.bc(x.t1,logg1,x.met,x.kw1)
		bc2=bb.bc(x.t2,logg2,x.met,x.kw2)
	
		mag1=bb.calcMag(x.l1,bc1)[0]
		mag2=bb.calcMag(x.l2,bc2)[0]
		#print mag1,mag2
	
		#print x.kep[i].t1,x.kep.logg1[i],x.kep.met[i],x.kep.kw1[i]
		#print x.kep[i].t2,x.kep.logg2[i],x.kep.met[i],x.kep.kw2[i]
		print mag1[0],ldc11[0][0],ldc12[0][0],gd1[0][0],mag2[0],ldc21[0][0],ldc22[0][0],gd2[0][0]

	