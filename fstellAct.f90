MODULE stellOscill
	IMPLICIT NONE
	!New tatic follow chaplin 2011
	DOUBLE PRECISION,PARAMETER :: lcFreq=1.d0/(60.d0*30.d0)
	!in uHz
	DOUBLE PRECISION,PARAMETER :: lcNy=lcFreq*0.5*10**6
	!uHz
	DOUBLE PRECISION,PARAMETER :: nuSun=3150.d0
	DOUBLE PRECISION,PARAMETER :: tredSun=8907.d0
	DOUBLE PRECISION,PARAMETER :: teffSun=5777.d0
	DOUBLE PRECISION,PARAMETER :: delT=1550.d0
	DOUBLE PRECISION,PARAMETER :: pi=3.141592654

	CONTAINS

	SUBROUTINE tred(lum,res,n)
		IMPLICIT NONE
		INTEGER,INTENT(IN) :: n
		DOUBLE PRECISION,DIMENSION(n),INTENT(IN) :: lum
		DOUBLE PRECISION,DIMENSION(n),INTENT(OUT) :: res
		!f2py INTENT(IN) :: lum
		!f2pt INTENT(OUT) :: res
		!f2py depend(n) :: res,lum
		res=tredSun*lum**(-0.093)
	END SUBROUTINE tred

	SUBROUTINE nuMax(rad,teff,res,n)
		IMPLICIT NONE
		INTEGER,INTENT(IN) :: n
		DOUBLE PRECISION,DIMENSION(n),INTENT(IN) :: rad,teff
		DOUBLE PRECISION,DIMENSION(n),INTENT(OUT) :: res
		!f2py INTENT(IN) :: rad,teff
		!f2pt INTENT(OUT) :: res
		!f2py depend(n) :: res,rad,teff
		res=nuSun*rad**(-2)*(teff/teffSun)
	END SUBROUTINE nuMax
	
	SUBROUTINE beta(teff,lum,res,n)
		IMPLICIT NONE
		INTEGER,INTENT(IN) :: n
		DOUBLE PRECISION,DIMENSION(n),INTENT(IN) :: teff,lum
		DOUBLE PRECISION,DIMENSION(n),INTENT(OUT) :: res
		DOUBLE PRECISION,DIMENSION(n) :: tr
		!f2py INTENT(IN) :: teff,lum
		!f2pt INTENT(OUT) :: res
		!f2py depend(n) :: res,tr,teff,lum
		CALL tred(lum,tr,n)
		res=1.0-exp(-(tr-teff)/delT)
	END SUBROUTINE beta

	SUBROUTINE neta(rad,teff,res,n)
		IMPLICIT NONE
		INTEGER,INTENT(IN) :: n
		DOUBLE PRECISION,DIMENSION(n),INTENT(IN) :: rad,teff
		DOUBLE PRECISION,DIMENSION(n),INTENT(OUT) :: res
		DOUBLE PRECISION,DIMENSION(n) :: nu,x
		!f2py INTENT(IN) :: rad,teff
		!f2pt INTENT(OUT) :: res
		!f2py depend(n) :: res,x,rad,teff
		CALL nuMax(rad,teff,nu,n)
		x=0.5*nu/lcNy
		res=sin(pi*x)/(x*pi)
	END SUBROUTINE neta	

	SUBROUTINE mass2bv(mass,res,n)
		IMPLICIT NONE
		INTEGER,INTENT(IN) :: n
		DOUBLE PRECISION,DIMENSION(n),INTENT(IN) :: mass
		DOUBLE PRECISION,DIMENSION(n),INTENT(OUT) :: res
		!f2py INTENT(IN) :: mass
		!f2pt INTENT(OUT) :: res
		!f2py depend(n) :: res,mass
		res=(log10(mass)-0.28)/(-0.42)
	END SUBROUTINE mass2bv	

	SUBROUTINE pSolar(rad,teff,lum,res,n)
		IMPLICIT NONE
		INTEGER,INTENT(IN) :: n
		DOUBLE PRECISION,DIMENSION(n),INTENT(IN) :: rad,teff,lum
		DOUBLE PRECISION,DIMENSION(n),INTENT(OUT) :: res
		DOUBLE PRECISION,DIMENSION(n) :: nu,b
		!f2py INTENT(IN) :: rad,teff
		!f2pt INTENT(OUT) :: res
		!f2py depend(n) :: res,nu,b,rad,teff
		CALL beta(teff,lum,b,n)
		CALL neta(rad,teff,nu,n)
		res=225.0*b**(2)*nu**(2)*rad**(3.5)*(teff/teffSun)**(1.25)
	END SUBROUTINE pSolar

	SUBROUTINE pGran(rad,teff,res,n)
		IMPLICIT NONE
		INTEGER,INTENT(IN) :: n
		DOUBLE PRECISION,DIMENSION(n),INTENT(IN) :: rad,teff
		DOUBLE PRECISION,DIMENSION(n),INTENT(OUT) :: res
		DOUBLE PRECISION,DIMENSION(n) :: nu
		!f2py INTENT(IN) :: rad,teff
		!f2pt INTENT(OUT) :: res
		!f2py depend(n) :: res,nu,rad,teff,mass
		CALL nuMax(rad,teff,nu,n)
		res=0.1*(nu/nuSun)**(-2)
	END SUBROUTINE pGran

	SUBROUTINE protAigran(mass,age,res,n)
		IMPLICIT NONE
		INTEGER,INTENT(IN) :: n
		DOUBLE PRECISION,DIMENSION(n),INTENT(IN) :: mass,age
		DOUBLE PRECISION,DIMENSION(n),INTENT(OUT) :: res
		DOUBLE PRECISION,DIMENSION(n) :: bv,a
		!f2py INTENT(IN) :: mass,age
		!f2pt INTENT(OUT) :: res
		!f2py depend(n) :: res,mass,age,bv,a
		CALL mass2bv(mass,bv,n)
		a=0.5*log10(age/0.625)
		WHERE(bv<0.45) bv=0.45
		WHERE((0.45<=bv).and.(bv<0.62)) res=-0.669+2.580*bv+a
		WHERE((bv>=0.62)) res=0.725+0.326*bv+a
		res=10**res
	END SUBROUTINE protAigran

	SUBROUTINE tauRot(mass,res,n)
		IMPLICIT NONE
		INTEGER,INTENT(IN) :: n
		DOUBLE PRECISION,DIMENSION(n),INTENT(IN) :: mass
		DOUBLE PRECISION,DIMENSION(n),INTENT(OUT) :: res
		DOUBLE PRECISION,DIMENSION(n) :: bv,x
		!f2py INTENT(IN) :: mass
		!f2pt INTENT(OUT) :: res
		!f2py depend(n) :: res,mass,bv,x
		CALL mass2bv(mass,bv,n)
		x=1.0-bv

		WHERE(x>0.0) res=1.362-0.166*x+0.025*x**2-5.323*x**3
		WHERE(x<0.0) res=1.362-0.14*x
		WHERE(bv>1.4) res=1.4
		res=10**res
	END SUBROUTINE tauRot



END MODULE stellOscill

! def rhk(mass,age):
! 	#noyes 1984
! 	return 6.0*10**(-5)*np.exp(-0.9*protAigran(mass,age)/tauRot(mass))
! 
! def sigmaAct(mass,age):
! 	#returns in ppm
! 	r=rhk(mass,age)
! 	res=np.zeros(np.size(mass))
! 	res[r>0.0]=6.0*10**(10.5+1.75*np.log10(r[r>0.0]))
! 	return res
! 
! def psd(nu,sig,tau):
! 	return 2.0*sig**(2)*tau/(1.0+(2.0*np.pi*nu*tau)**(2))
! 
! def pAct(mass,rad,teff,age):
! 	sig=sigmaAct(mass,age)
! 	tau=8.0*3600.0*24.0
! 
! 	intFreq=np.zeros(np.size(sig))
! 	for i in range(0,len(sig)):
! 		intFreq[i]=sci.romberg(psd,0.13,555.0,args=(sig[i],tau),divmax=15,rtol=10**-10,vec_func=True)
! 	
! 	return intFreq
! 
! def solar(mass,rad,teff,lum,age):
! 	return np.sqrt((pGran(rad,teff)+pSolar(mass,rad,teff,lum)+pAct(mass,rad,teff,age))*nuMax(rad,teff))*10**(-6)
! 
! 
! 
! #GB,Che and AGB fro hekker 2012 in absence of other data we pick random mu max
! def gb(mass):
! 	#in microHz
! 	minV=1.0
! 	maxV=200.0
! 	#Pick numbers linearly from log space
! 	res=10**(np.random.uniform(np.log10(minV),np.log10(maxV),size=np.size(mass)))
! 	res=(3.5*res**(-1.28)+1.4*res**(-1.5))*10**7
! 	
! 	return np.sqrt(res)*10**-6
! 
! 	
! def che(mass):
! 	#aka RC stars
! 	#in microHz
! 	minV=20.0
! 	maxV=40.0
! 	#Pick numbers linearly from log space
! 	res=10**(np.random.uniform(np.log10(minV),np.log10(maxV),size=np.size(mass)))
! 	res=(3.5*res**(-1.28)+1.4*res**(-1.5))*10**7
! 	
! 	return np.sqrt(res)*10**-6
! 
! def agb(mass):
! 	#in microHz
! 	minV=1.0
! 	maxV=20.0
! 	#Pick numbers linearly from log space
! 	res=10**(np.random.uniform(np.log10(minV),np.log10(maxV),size=np.size(mass)))
! 	res=(3.5*res**(-1.28)+1.4*res**(-1.5))*10**7
! 	
! 	return np.sqrt(res)*10**-6
! 
! #Random guassian centered on 1% with sigma of 0.5%, clipped such that the min varaince is 0.5%
! def wd(mass):
! 	ran=np.random.randn(np.size(mass))*0.5+1.0
! 	ran[ran<0.5]=0.5
! 	return ran/100.0
! 
! def stellOscill(mass,rad,teff,lum,age,sType):
! 	#sTypes 0,1 MS 2-9 GB/CHe/AGB/nHe, 10-12 WD, 13 NS
! 
! 	res=np.zeros(np.size(mass))
! 
! 	#Zero noise for WD
! 	indWD=((sType>=10)&(sType<=12))
! 	res[indWD]=wd(mass)
! 	#Zero noise for NS
! 	indNS=(sType==13)
! 	res[indNS]=wd(indNS)
! 
! 	
! 	#For solar type stars
! 	indMS=((sType==0)|(sType==1))
! 	res[indMS]=solar(mass[indMS],rad[indMS],teff[indMS],lum[indMS],age[indMS])
! 
! 	#HG+GB
! 	indGB=((sType==2)|(sType==3))
! 	res[indGB]=gb(mass[indGB])
! 		
! 	#CHe
! 	indCHe=(sType==4)
! 	res[indCHe]=che(mass[indCHe])
! 
! 	#AGB+TPAGB?
! 	indAGB=((sType==5)|(sType==6))
! 	res[indAGB]=agb(mass[indAGB])
! 		
! 	return res
